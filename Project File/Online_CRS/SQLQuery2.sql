﻿use crp
alter table car_details alter column car_name varchar(30) not null
alter table car_details alter column fueltype varchar(10) not null
alter table car_details alter column comfort  float not null 
alter table car_details alter column safeties float not null
alter table car_details alter column quality float not null
alter table car_details alter column media_feature  float not null
alter table car_details alter column city_mileage  float not null
alter table car_details alter column high_mileage float not null
alter table car_details alter column cargo_volume  float not null
alter table car_details alter column seating_capacity  float not null
alter table car_details alter column speed  float not null
alter table car_details alter column price  float not null