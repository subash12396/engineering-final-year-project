using System;
using System;


public class Test
{
       static int[,] n_dist=new int[5,5] {{-1,510,354,411,334},
		                {510,-1,167,101,217},
		                {354,167,-1,76,186},
		                {411,101,76,-1,148},
		                {334,17,186,148,-1}};
		                
	  static string[,] n_no=new string[5,5] {{"N/A","NH-132/79","NH-132","NH-79","NH-132"},
	    	                {"NH-132/79","N/A","NH-544","NH-544","NH-81"},
		                    {"NH-132","NH-544","N/A","NH-544","NH-44/81"},
		                    {"NH-79","NH-544","NH-544","N/A","sh-25"},
		                    {"NH-132","NH-81","NH-44/81","sh-25","N/A"}};
		                    
	  static  string[,] n_traffic=new string[5,5] {{"N/A","AH-45","NH-132","AH45","NH-132"},
		                       {"AH-45","N/A","NH-544","NH-544","NH-81"},
	               	             {"NH-132","NH-544","N/A","NH-544","NH-44/81"},
		                  {"AH-45","NH-544","NH-544","N/A","sh-25"},
		                    {"NH-132","NH-81","NH-44/81","sh-25","N/A"}};
		                    
		static int[,] s_dist=new int[5,5] {{-1,510,331,397,396},
		  {510,-1,167,119,236},
		  {331,167,-1,68,146},
		  {397,119,68,-1,148},
		  {396,236,146,148,-1}};
		  
	static	string[,] s_no=new string[5,5] {{"N/A","NH-132/79","SH-18","SH-18/544","SH-9/38"},
		  {"NH-132/79","N/A","NH-544","SH-96","SH-21"},
		  {"SH-81","NH-544","N/A","SH-79A","SH-25"},
		  {"SH-18/544","SH-96","SH-79A","N/A","SH-25"},
		  {"SH-9/38","SH-21","SH-25","SH-25","N/A"}};
static string[,] s_low=new string[5,5] {{"N/A","AH-45","SH-18","SH-18","SH-9"},
		   {"AH-45","N/A","NH-544","SH-96","SH-21"},
		   {"SH-18","NH-544","N/A","SH-79A","SH-21"},
		   {"SH-18","SH-96","SH-79A","N/A","SH-25"},
		   {"SH-9","SH-21","SH-25","SH-25","N/A"}};
	public static void Main()
	{
	    int start,end,note=0;
        int[] inter;
        int[] route=new int[30];
        int[] sh=new int[30];
        int[] nh=new int[30]; 
        int s,n,i,j,choice,t,low,r_i=0,check=0;
        Console.WriteLine("enter the staring location 0,1,2,3,4,5");
        start=Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("enter the ending location 0,1,2,3,4,5");
        end=Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("how many intermediate sates you want to travel?");
        n=Convert.ToInt32(Console.ReadLine());
        inter=new int[n+1] ;
        Console.WriteLine("Enter ");
        
        for( i=0;i<n;i++)
	    {
        	inter[i]=Convert.ToInt32(Console.ReadLine());
	    }
        Console.WriteLine("enter parameters 1.nh ,2.sh");
        choice=Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("do you want low traffic?1/0");
        t=Convert.ToInt32(Console.ReadLine());
        if(choice==1 && t==0)
	    {
	        s=start;
	        for(int k=0;k<n;k++)
	        {    
	               low=5000;
		           for(i=0;i<n;i++)
	    	       {
			            if(inter[i]!=-1)
			            {
				            if((n_dist[s,inter[i]])<low&&(n_dist[s,inter[i]])!=-1)
                            {
					            low=n_dist[s,inter[i]];
					            note=i;
				            }
			            }
		            }
		            route[r_i]=inter[note];r_i++;
		            s=inter[note];
		            inter[note]=-1;
	       }
	        route[r_i]=end;
	        s=start;
		    Console.WriteLine("the route is");
		    for(i=0;i<=r_i;i++)
		    {
		           Console.WriteLine(n_no[s,route[i]]) ;
		            s=route[i];
		    }
	    }
	    if(choice==1&&t==1)
	    {   
	        s=start;
	        for(int k=0;k<n;k++)
	        {      
	            low=5000;
		        for(i=0;i<n;i++)
		        {
		    	    if(inter[i]!=-1)
			        {
				        if((n_dist[s,inter[i]])<low&&(n_dist[s,inter[i]])!=-1)
				        {
					        low=n_dist[s,inter[i]];
					        note=i;
			        	}
			        }
		        }
		        route[r_i]=inter[note];r_i++;
		        s=inter[note];
		        inter[note]=-1;
	       }
	       route[r_i]=end;
	       s=start;
	       Console.WriteLine("the route is");
		   for(i=0;i<=r_i;i++)
		   {
		       Console.WriteLine(n_traffic[s,route[i]]) ;
		       s=route[i];
		   }
	    }
	    if(choice==2&&t==0)
	    {
            s=start;
	        for(int k=0;k<n;k++)
	        {  
	            low=5000;
		        for(i=0;i<n;i++)
		        {
			        if(inter[i]!=-1)
			        {
				        if((s_dist[s,inter[i]])<low&&(s_dist[s,inter[i]])!=-1)
				        {
					        low=s_dist[s,inter[i]];
					        note=i;
				        }
			        }
		        }
		        route[r_i]=inter[note];r_i++;
		        s=inter[note];
		        inter[note]=-1;
	       }
	       route[r_i]=end;
	       s=start;
	        Console.WriteLine("the route is");
		    for(i=0;i<=r_i;i++)
		    {
		       Console.WriteLine(s_no[s,route[i]]) ;
		       s=route[i];
		    }
	    }
	    if(choice==2&&t==1)
	{

	  s=start;
	  for(int k=0;k<n;k++)
	  {  low=5000;
		for(i=0;i<n;i++)
		{
			if(inter[i]!=-1)
			{
				if((s_dist[s,inter[i]])<low&&(s_dist[s,inter[i]])!=-1)
				{
					low=s_dist[s,inter[i]];
					note=i;
				}
			}
		}
		route[r_i]=inter[note];r_i++;
		s=inter[note];
		inter[note]=-1;

	  }
	  route[r_i]=end;
	  s=start;
		 Console.WriteLine("the route is");
		 for(i=0;i<=r_i;i++)
		 {
		       Console.WriteLine(s_low[s,route[i]]) ;
		       s=route[i];
		 }
	}
	    
    }
    
}
