﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace Online_CRS
{
    public partial class adminlogin : System.Web.UI.Page
    {
        static string email, password;

        public adminhome adminhome
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public adminsignup adminsignup
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

       

        protected void b_adminregister_Click(object sender, EventArgs e)
        {
            Server.Transfer("adminsignup.aspx");

        }

        protected void b_adminlogin_Click(object sender, EventArgs e)
        {
            Label5.Visible = false;
            email = tb_adminlogin.Text.ToString();
            password = tb_adminpassword.Text.ToString();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                conn.Open();
                SqlCommand comm = new SqlCommand("select admin_pass from admindetails where admin_email=@email and admin_pass=@password", conn);
                comm.Parameters.AddWithValue("@email", email);
                comm.Parameters.AddWithValue("@password", password);
                SqlDataReader reader = comm.ExecuteReader();
                if (reader.Read())
                {
                    Server.Transfer("adminhome.aspx");
                }
                else
                {
                    Label5.Visible = true;
                    tb_adminlogin.Text = "";
                }
            }

        }
    }
}