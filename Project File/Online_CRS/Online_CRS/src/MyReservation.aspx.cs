﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
namespace Online_CRS
{
    public partial class MyReservation : System.Web.UI.Page
    {
        string emailid;

        public Profile Profile
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["emailid"] != null)
                emailid = Session["emailid"].ToString();
            using (SqlConnection con = new SqlConnection())
            {

                con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                con.Open();
                SqlCommand com = new SqlCommand("select * from orderdetails where user_emailid=@emailid", con);
                com.Parameters.AddWithValue("@emailid", emailid);
                //tb_view.Text=com.ExecuteScalar().ToString();
                SqlDataReader nwReader = com.ExecuteReader();
                String data = "";
                while (nwReader.Read())
                {
                    data = data + "\t" + nwReader["pickup_location"].ToString();
                    data = data + "\t" + nwReader["drop_location"].ToString();
                    data = data + "\t" + nwReader["places_to_visit"].ToString();
                    data = data + "\t" + nwReader["pickup_date"].ToString();
                    data = data + "\t" + nwReader["pickup_time"].ToString();
                    data = data + "\t" + nwReader["drop_time"].ToString();
                    data = data + "\t" + nwReader["drop_date"].ToString();
                    data = data + "\t" + nwReader["driver_req"].ToString();
                    data = data + "\t" + nwReader["driver_type"].ToString();
                    data = data + "\t" + nwReader["driver_experience"].ToString();
                    data = data + "\t" + nwReader["selected_car"].ToString();
                    data = data + "\t" + nwReader["car_id"].ToString();
                    data = data + "\t" + nwReader["amount"].ToString();
                    data = data + "\n";

                }

                nwReader.Close();
                com.ExecuteNonQuery();
                tb_details.Text = data.ToString();
            }

        }
        protected void bt_okay_Click(object sender, EventArgs e)
        {
            Server.Transfer("Profile.aspx");
        }

        protected void bt_cancelreservation_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection())
            {

                con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                con.Open();
                SqlCommand com = new SqlCommand("delete from orderdetails where user_emailid=@email", con);
                com.Parameters.AddWithValue("@email", emailid);
                com.ExecuteNonQuery();
                string script = "alert('Reservations are Cancelled Successfully');";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
            }
        }
    }
}
