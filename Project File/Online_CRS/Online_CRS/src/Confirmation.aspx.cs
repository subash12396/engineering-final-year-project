﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
namespace Online_CRS
{
    public partial class Confirmation : System.Web.UI.Page
    {
        int order_id;
        string emailid;

        public booking booking
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Session["order_id"] != null)
                order_id = Convert.ToInt32(Session["order_id"]);
            if (Session["emailid"] != null)
                emailid = Session["emailid"].ToString();
            using (SqlConnection con = new SqlConnection())
            {

                con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                con.Open();
                SqlCommand com = new SqlCommand("select * from orderdetails where order_id=@id", con);
                com.Parameters.AddWithValue("@id", order_id);
                SqlDataReader nwReader = com.ExecuteReader();
                while (nwReader.Read())
                {
                    tb_source.Text = nwReader["pickup_location"].ToString();
                    tb_destination.Text = nwReader["drop_location"].ToString();
                    tb_selectedcity.Text = nwReader["places_to_visit"].ToString();
                    tb_fromtime.Text = nwReader["pickup_time"].ToString();
                    tb_totime.Text = nwReader["drop_time"].ToString();
                    tb_fromdate.Text = nwReader["pickup_date"].ToString();
                    tb_todate.Text = nwReader["drop_date"].ToString();
                    tb_driverdetails.Text = nwReader["driver_req"].ToString() + " " + nwReader["driver_type"].ToString() + " " + nwReader["driver_experience"].ToString();
                    tb_selectedcar.Text = nwReader["selected_car"].ToString();
                    tb_amount.Text = nwReader["amount"].ToString();
                }

                nwReader.Close();
                com.ExecuteNonQuery();

            }
        }
        protected void bt_booknow_Click(object sender, EventArgs e)
        {

            double amount = Convert.ToDouble(tb_amount.Text.ToString());
            using (SqlConnection con = new SqlConnection())
            {

                con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                con.Open();
                SqlCommand com = new SqlCommand("insert paymentdetails values(@user_emailid,@amount,@order_id)", con);
                com.Parameters.AddWithValue("@user_emailid", emailid);
                com.Parameters.AddWithValue("@amount", amount);
                com.Parameters.AddWithValue("@order_id", order_id);
                com.ExecuteNonQuery();
            }
            string script = "alert('Your order has done successfully');";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
            Server.Transfer("booking.aspx");

        }
        protected void bt_modify_Click(object sender, EventArgs e)
        {
            Server.Transfer("booking.aspx");
        }

        protected void bt_logout_Click(object sender, EventArgs e)
        {

            Session.Clear();
            Session.Abandon();
            Server.Transfer("userhome.aspx");
        }
    }
}
