﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site(admin).Master" AutoEventWireup="true" CodeBehind="adminsignup.aspx.cs" Inherits="Online_CRS.adminsignup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>

   </div>'
    <div>

        <table class="auto-style5">
            <tr>
                <td class="auto-style13">
                    <asp:Label ID="Label1" runat="server" style="margin-left:250px" Text="Company Name"></asp:Label>
                </td>
                <td class="auto-style14">
                    <asp:TextBox ID="tb_companyname" runat="server" Width="315px" CssClass="auto-style20"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tb_companyname" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style9">
                    <asp:Label ID="Label2" runat="server" style="margin-left:250px" Text="Name"></asp:Label>
                </td>
                <td class="auto-style10">
                    <asp:TextBox ID="tb_adminname" runat="server" Width="315px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tb_adminname" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style13">
                    <asp:Label ID="Label3" runat="server" style="margin-left:250px" Text="Email ID"></asp:Label>
                </td>
                <td class="auto-style14">
                    <asp:TextBox ID="tb_adminemailid" runat="server"   Width="315px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="tb_adminemailid" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style13">
                    <asp:Label ID="Label4" runat="server" style="margin-left:250px"  Text="Password"></asp:Label>
                </td>
                <td class="auto-style14">
                    <asp:TextBox ID="tb_adminpassword" runat="server"  Width="315px" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="tb_adminpassword" ErrorMessage="*"></asp:RequiredFieldValidator>
                    <br />
                </td>
            </tr>
            <tr>
                <td class="auto-style13">
                    <asp:Label ID="Label5" runat="server"  style="margin-left:250px" Text="Confirm Password"></asp:Label>
                </td>
                <td class="auto-style14">
                    <asp:TextBox ID="tb_adminconpassword"  runat="server" Width="315px" TextMode="Password" CssClass="auto-style17"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="tb_adminconpassword" ErrorMessage="*"></asp:RequiredFieldValidator>
                    <br />
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="tb_adminpassword" ControlToValidate="tb_adminconpassword" ErrorMessage="Password Mismatch"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style6">
                    <asp:Label ID="Label6" runat="server" style="margin-left:250px"  Text="Company Address"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tb_comaddress" runat="server" Width="315px" Height="49px" TextMode="MultiLine"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="tb_comaddress" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style13">
                    <asp:Label ID="Label7" runat="server" style="margin-left:250px"  Text="Phone no."></asp:Label>
                </td>
                <td class="auto-style14">
                    <asp:TextBox ID="tb_adminphoneno" runat="server" Width="315px" Height="19px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="tb_adminphoneno" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
           
            <tr>
                <td class="auto-style18">
                    </td>
                <td class="auto-style19">
                    </td>
            </tr>
           
        </table>

    </div>
    <div>

        <asp:Button ID="b_adminsignup" style="margin-left:500px" runat="server" Text="Sign up" OnClick="b_adminsignup_Click1" Width="69px" />
        

        <br />
        

        <br />
        

        <asp:Label ID="Label8" runat="server" style="margin-left:450px" Text="--------------OR--------------"></asp:Label>
        <br />
        <br />
        

    </div>
    <div>

        <asp:Button ID="b_adminlogin" style="margin-left:500px" runat="server" Text="Login" OnClick="b_adminlogin_Click" Width="70px" CausesValidation="False" />

        <br />

    </div>
</asp:Content>
