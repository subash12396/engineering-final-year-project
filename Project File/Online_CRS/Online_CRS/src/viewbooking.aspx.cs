﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace Online_CRS
{
    public partial class viewbooking : System.Web.UI.Page
    {
        public adminhome adminhome
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection())
            {

                con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                con.Open();
                SqlCommand com = new SqlCommand("select * from orderdetails", con);
                //tb_view.Text=com.ExecuteScalar().ToString();
                SqlDataReader nwReader = com.ExecuteReader();
                String data = "";
                while (nwReader.Read())
                {

                    data = data + "\t" + nwReader["user_emailid"].ToString();
                    data = data + "\t" + nwReader["pickup_location"].ToString();
                    data = data + "\t" + nwReader["drop_location"].ToString();
                    data = data + "\t" + nwReader["places_to_visit"].ToString();
                    data = data + "\t" + nwReader["pickup_date"].ToString();
                    data = data + "\t" + nwReader["pickup_time"].ToString();
                    data = data + "\t" + nwReader["drop_time"].ToString();
                    data = data + "\t" + nwReader["drop_date"].ToString();
                    data = data + "\t" + nwReader["driver_req"].ToString();
                    data = data + "\t" + nwReader["driver_type"].ToString();
                    data = data + "\t" + nwReader["driver_experience"].ToString();
                    data = data + "\t" + nwReader["selected_car"].ToString();
                    data = data + "\t" + nwReader["car_id"].ToString();
                    data = data + "\t" + nwReader["amount"].ToString();
                    data = data + "\n";

                }
                tb_view.Text = data.ToString();
                nwReader.Close();
                com.ExecuteNonQuery();
            }
        }
            
        protected void b_viewbookingdone_Click(object sender, EventArgs e)
        {
            Server.Transfer("adminhome.aspx");
        }
    }
}