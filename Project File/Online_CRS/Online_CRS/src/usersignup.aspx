﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site(user login signup).Master" AutoEventWireup="true" CodeBehind="usersignup.aspx.cs" Inherits="Online_CRS.usersignup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 893px;
            height: 285px;
        }
        .auto-style2 {
            width: 368px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div "auto-style35">
    <table class="auto-style1">
        <tr>
            <td class="auto-style2">
                <asp:Label ID="lblname0" runat="server"  style="margin-left:310px" Text="Name:" CssClass="auto-style40" ></asp:Label>
            </td>
            <td class="auto-style32">
                <asp:TextBox ID="tb_name" runat="server" Width="200px" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tb_name" ErrorMessage="*" ForeColor="#000099"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Label ID="lblemailid" runat="server" Text="Email Id:" CssClass="auto-style41" style="margin-left:300px" ></asp:Label>
            </td>
            <td class="auto-style32">
                <asp:TextBox ID="tb_emailid" runat="server" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="tb_emailid" ErrorMessage="*" ForeColor="#000099"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="tb_emailid" ErrorMessage="Please enter your valid email id" ForeColor="#000099" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Label ID="lblpassword" runat="server" style="margin-left:290px" Text="Password:"></asp:Label>
            </td>
            <td class="auto-style32">
                <asp:TextBox ID="tb_password" runat="server" TextMode="Password" Width="200px" CssClass="auto-style33"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="tb_password" ErrorMessage="*" ForeColor="#000099"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Label ID="lblconfirmpassword" runat="server" Text="Confirm Password:" CssClass="auto-style37" style="margin-left:240px" ></asp:Label>
            </td>
            <td class="auto-style32">
                <asp:TextBox ID="tb_confirmpassword" runat="server" TextMode="Password" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="tb_confirmpassword" ErrorMessage="*" ForeColor="#000099"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="tb_password" ControlToValidate="tb_confirmpassword" ErrorMessage="password mismatch" ForeColor="#000099"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Label ID="lbladdress" runat="server" Text="Address:" CssClass="auto-style39" style="margin-left:300px" ></asp:Label>
            </td>
            <td class="auto-style32">
                <asp:TextBox ID="tb_address" runat="server"  Width="200px" Height="49px" TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tb_address" ErrorMessage="*" ForeColor="#000099"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Label ID="lblphoneno" runat="server" Text="Phone No:" CssClass="auto-style38" style="margin-left:290px"></asp:Label>
        
            </td>
            <td class="auto-style32">
                <asp:TextBox ID="tb_phoneno" runat="server" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="tb_phoneno" ErrorMessage="*" ForeColor="#000099"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="tb_phoneno" ErrorMessage="Please enter valid 10digit number" ForeColor="#000099" ValidationExpression="\d{10}"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style32">
                <asp:Button ID="b_signup" runat="server" OnClick="b_signup_Click" Text="SignUp" AutoPostback="true"/>
            </td>
        </tr>
    </table>
       </div>
    <div>

        <asp:Label ID="Label1" runat="server" Style="margin-left:300px" Text="------------------OR-----------------"></asp:Label>

    </div>
    <div>

        <asp:Button ID="b_login" style="margin-left:370px" runat="server" Text="Login" OnClick="b_login_Click" CausesValidation="False" />

    </div>
</asp:Content>
