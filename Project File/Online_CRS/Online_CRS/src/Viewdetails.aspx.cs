﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace Online_CRS
{
    public partial class Viewdetails : System.Web.UI.Page
    {
       int car_id;

        public UpdateDB UpdateDB
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["car_id_view"] != null)
            {
                car_id = Convert.ToInt32(Session["car_id_view"].ToString());
            }
            String data = " ";
            using (SqlConnection con = new SqlConnection())
            {

                con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                con.Open();
                SqlCommand com = new SqlCommand("select * from car_details where car_id=@car_id", con);
                com.Parameters.AddWithValue("@car_id", car_id);
                SqlDataReader nwReader = com.ExecuteReader();
                while (nwReader.Read())
                {
                    data = data + nwReader["car_name"].ToString();
                    data = data + "  " + nwReader["comfort"].ToString();
                    data = data + "  " + nwReader["safeties"].ToString();
                    data = data + "  " + nwReader["quality"].ToString();
                    data = data + "  " + nwReader["media_feature"].ToString();
                    data = data + "  " + nwReader["city_mileage"].ToString();
                    data = data + "  " + nwReader["high_mileage"].ToString();
                    data = data + "  " + nwReader["Cargo_volume"].ToString();
                    data = data + "  " + nwReader["Seating_capacity"].ToString();
                    data = data + "  " + nwReader["Speed"].ToString();
                    data = data + "  " + nwReader["Price"].ToString();
                    data = data + "\n";
                }
                nwReader.Close();
                com.ExecuteNonQuery();
            }
            tb_view.Text = data;

        }

        protected void b_done_Click(object sender, EventArgs e)
        {
            Server.Transfer("UpdateDB.aspx");
        }
    }
}