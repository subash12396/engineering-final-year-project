﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site(admin).Master" AutoEventWireup="true" CodeBehind="UpdateDB.aspx.cs" Inherits="Online_CRS.UpdateDB" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 23px;
        }
        .auto-style3 {
            height: 23px;
            width: 169px;
        }
        .auto-style4 {
            width: 169px;
        }
        .auto-style5 {
            width: 172px;
        }
        .auto-style6 {
            width: 174px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>

        <asp:Label ID="Label2" runat="server" Text="Add New Car"></asp:Label>

    </div>
    <div>

        <table class="auto-style1">
            <tr>
                <td class="auto-style3">
                    <asp:Label ID="Label3" runat="server" Text="Car id"></asp:Label>
                </td>
                <td class="auto-style2">
                    <asp:TextBox ID="tb_carid" runat="server" TextMode="Number"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tb_carid" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label4" runat="server" Text="Car Name"></asp:Label>
                </td>
                <td id="tb">
                    <asp:TextBox ID="tb_carname" runat="server" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tb_carname" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label5" runat="server" Text="Fuel Type"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tb_fueltype" runat="server" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="tb_fueltype" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label6" runat="server" Text="Comfort"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tb_comfort" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="tb_comfort" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label7" runat="server" Text="Safety"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tb_safety" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="tb_safety" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label8" runat="server" Text="Quality"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tb_quality" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="tb_quality" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label9" runat="server" Text="Media Features"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tb_mediafeatures" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="tb_mediafeatures" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label10" runat="server" Text="City Mileage"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tb_citymileage" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="tb_citymileage" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label11" runat="server" Text="HighWay Mileage"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tb_highwaymileage" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="tb_highwaymileage" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label12" runat="server" Text="Cargo Volume"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tb_cargovolume" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="tb_cargovolume" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="laa" runat="server" Text="Seating Capacity"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tb_seatingcap" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="tb_seatingcap" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label14" runat="server" Text="High Speed"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tb_speed" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="tb_speed" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label15" runat="server" Text="Price per hour"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tb_price" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="tb_price" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">&nbsp;</td>
                <td>
                    <asp:Button ID="b_update" runat="server" Text="Update" OnClick="b_update_Click" />
                </td>
            </tr>
            <tr>
                <td class="auto-style4">&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>

    </div>
    <div>

        <asp:Label ID="Label16" runat="server" Text="Delete Car"></asp:Label>

    </div>
    <div>
       
                   
                  
    </div>
    <div>

        <table class="auto-style1">
            <tr>
                <td class="auto-style5">
                    <asp:Label ID="Label17" runat="server" Text="Car id"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tb_del_carid" runat="server" TextMode="Number"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style5">&nbsp;</td>
                <td>
                    <asp:Button ID="b_delete" runat="server" Text="Delete" CausesValidation="False" OnClick="b_delete_Click" />
                </td>
            </tr>
            <tr>
                <td class="auto-style5">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>

    </div>
    <div>

        <asp:Label ID="Label18" runat="server" Text="View Details"></asp:Label>

    </div>
    <div>

        <table class="auto-style1">
            <tr>
                <td class="auto-style6">
                    <asp:Label ID="Label19" runat="server" Text="Car id"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tb_view_carid" runat="server" TextMode="Number"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style6">&nbsp;</td>
                <td>
                    <asp:Button ID="b_view" runat="server" Text="View" OnClick="b_view_Click" CausesValidation="False" />
                </td>
            </tr>
            <tr>
                <td class="auto-style6">
                    <asp:Label ID="Label20" runat="server" Text="View all Car Details"></asp:Label>
                </td>
                <td>
                    <asp:Button ID="b_viewall" runat="server" Text="View All" OnClick="b_viewall_Click" CausesValidation="False" />
                </td>
            </tr>
        </table>

    </div>
</asp:Content>
