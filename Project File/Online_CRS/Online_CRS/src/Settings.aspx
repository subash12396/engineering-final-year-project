﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site(user login signup).Master" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="Online_CRS.Settings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 578px;
            height: 268px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
    
        <asp:Label ID="Label1" style="margin-left:100px" runat="server" Text="Settings"></asp:Label>
    
    </div>
    <div>

            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lbl_name" runat="server" Text="Name"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="tb_name" runat="server" Width="180px"></asp:TextBox>

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="tb_name" ErrorMessage="*"></asp:RequiredFieldValidator>

                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lbl_newpassword" runat="server" Text="New Password"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="tb_newpassword" runat="server" Width="180px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tb_newpassword" ErrorMessage="*"></asp:RequiredFieldValidator>

                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lbl_confirmpassword" runat="server" Text="Confirm Password"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="tb_confirmpassword" runat="server" Width="180px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="tb_confirmpassword" ErrorMessage="*"></asp:RequiredFieldValidator>
                        <br />
                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToCompare="tb_newpassword" ControlToValidate="tb_confirmpassword" ErrorMessage="password mismatch"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lbl_address" runat="server" Text="Address"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="tb_address" runat="server" TextMode="MultiLine" Width="180px" Height="39px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="tb_address" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lbl_phone" runat="server" Text="Phone"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="tb_phone" runat="server" Width="180px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="tb_phone" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>

            </div>
        <div>
            
            <asp:Button ID="b_back" runat="server" CausesValidation="False" OnClick="b_back_Click" Text="Back" />
            
            <asp:Button ID="b_update" runat="server" style="margin-left:300px" OnClick="b_update_Click" Text="update" />
            
        &nbsp;</div>
</asp:Content>
