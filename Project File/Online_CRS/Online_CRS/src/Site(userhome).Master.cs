﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Online_CRS
{
    public partial class Site_userhome_ : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void b_login_Click(object sender, EventArgs e)
        {
            Server.Transfer("userlogin.aspx");
        }

        protected void b_signup_Click(object sender, EventArgs e)
        {
            Server.Transfer("usersignup.aspx");
        }
    }
}