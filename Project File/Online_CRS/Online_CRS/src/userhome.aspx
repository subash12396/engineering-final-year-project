﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site(userhome).Master" AutoEventWireup="true" CodeBehind="userhome.aspx.cs" Inherits="Online_CRS.userhome" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
            width: 72%;
            height: 96px;
            margin-left: 151px;
        }
        .auto-style3 {
            width: 234px;
        }
        .auto-style4 {
            width: 234px;
            height: 28px;
        }
        .auto-style5 {
            height: 28px;
        }
        .auto-style6 {
            width: 234px;
            height: 23px;
        }
        .auto-style7 {
            height: 23px;
        }
        .auto-style8 {
            width: 71%;
            height: 67px;
            margin-left: 153px;
        }
        .auto-style9 {
            width: 235px;
        }
        .auto-style10 {
            width: 70%;
            height: 25px;
            margin-left: 156px;
            margin-right: 0px;
        }
        .auto-style11 {
            width: 35px;
        }
        .auto-style12 {
            width: 35px;
            height: 23px;
        }
        .auto-style13 {
            width: 70%;
            height: 23px;
            margin-left: 157px;
        }
        .auto-style14 {
            width: 218px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div>
        <asp:Label ID="lblabout" style="margin-left:100px" runat="server" Text="About:"></asp:Label>
        </div>
    <div>

        <asp:TextBox ID="tb_about" style="margin-left:150px" runat="server" TextMode="MultiLine" Height="130px" Width="520px" >     The information technology nowadays is growing from time-to-time, therefore, the demand of using Internet is increasing year-by-year. In addition, most of the traditional companies are shifted their traditional business model into the modern business model, which is open an online store to public Internet users to purchase the goods or services online and advances in the internet services like 3G, 4G, etc., have paved way for increase in usage of internet and also the usage of maps while travelling, we tend to find it difficult to find an efficient way to travel through the states and to select an optimal vehicle for our travel.</asp:TextBox>

    </div>
    <div>
        <br />
        <asp:Label ID="lblpopular" style="margin-left:100px" runat="server" Text="Most popular cities  to visit:"></asp:Label>
        <br />
       </div>
       <div>

           <table class="auto-style2">
               <tr>
                   <td class="auto-style4">&nbsp;</td>
                   <td class="auto-style5">&nbsp;</td>
               </tr>
               <tr>
                   <td class="auto-style4">
                       <asp:Label ID="Label2" runat="server" Text="Source"></asp:Label>
                   </td>
                   <td class="auto-style5">
                       <asp:Label ID="Label3" runat="server" Text="Destination"></asp:Label>
                   </td>
               </tr>
               <tr>
                   <td class="auto-style3">
                       <asp:Label ID="Label4" runat="server" Text="Chennai"></asp:Label>
                   </td>
                   <td>
                       <asp:Label ID="Label7" runat="server" Text="Madurai"></asp:Label>
                   </td>
               </tr>
               <tr>
                   <td class="auto-style6">
                       <asp:Label ID="Label5" runat="server" Text="Trichy"></asp:Label>
                   </td>
                   <td class="auto-style7">
                       <asp:Label ID="Label8" runat="server" Text="Kaniyakumari"></asp:Label>
                   </td>
               </tr>
               <tr>
                   <td class="auto-style6">
                       <asp:Label ID="Label6" runat="server" Text="Thanjuvr"></asp:Label>
                   </td>
                   <td class="auto-style7">
                       <asp:Label ID="Label9" runat="server" Text="Villupuram"></asp:Label>
                   </td>
               </tr>
               <tr>
                   <td class="auto-style6">&nbsp;</td>
                   <td class="auto-style7">&nbsp;</td>
               </tr>
           </table>

       </div>
    <div>
        <br />
        <asp:Label ID="lblreviews" style="margin-left:100px" runat="server" Text="Reviews by the customers:"></asp:Label>
        <br />
        </div>
    <div>
         <table class="auto-style8">
             <tr>
                 <td class="auto-style9">&nbsp;</td>
                 <td>&nbsp;</td>
             </tr>
             <tr>
                 <td class="auto-style9">
                     <asp:Label ID="Label10" runat="server" Text="Car Names"></asp:Label>
                 </td>
                 <td>
                     <asp:Label ID="Label12" runat="server" Text="Rating"></asp:Label>
                 </td>
             </tr>
             <tr>
                 <td class="auto-style9">
                     <asp:Label ID="Label11" runat="server" Text="Toyota Platinum Etios JT"></asp:Label>
                 </td>
                 <td>
                     <asp:Label ID="Label13" runat="server" Text="4"></asp:Label>
                 </td>
             </tr>
             <tr>
                 <td class="auto-style9">
                     <asp:Label ID="Label14" runat="server" Text="Chevrolet Captiva"></asp:Label>
                 </td>
                 <td>
                     <asp:Label ID="Label18" runat="server" Text="5"></asp:Label>
                 </td>
             </tr>
             <tr>
                 <td class="auto-style9">
                     <asp:Label ID="Label15" runat="server" Text="Honda City i-DTEC SV"></asp:Label>
                 </td>
                 <td>
                     <asp:Label ID="Label17" runat="server" Text="3"></asp:Label>
                 </td>
             </tr>
             <tr>
                 <td class="auto-style9">&nbsp;</td>
                 <td>&nbsp;</td>
             </tr>
         </table>
    </div>
    <div>
        <asp:Label ID="lblfaq" style="margin-left:100px" runat="server" Text="FAQ:"></asp:Label>
        </div>
    <div>
         <table class="auto-style10">
             <tr>
                 <td class="auto-style11">&nbsp;</td>
                 <td>&nbsp;</td>
             </tr>
             <tr>
                 <td class="auto-style11">
                     <asp:Label ID="Label19" runat="server" Text="1"></asp:Label>
                 </td>
                 <td>
                     <asp:Label ID="Label23" runat="server" Text="Who should pay for fuel? "></asp:Label>
                 </td>
             </tr>
             <tr>
                 <td class="auto-style12">
                     <asp:Label ID="Label20" runat="server" Text="2"></asp:Label>
                 </td>
                 <td class="auto-style7">
                     <asp:Label ID="Label24" runat="server">Damage expenses if any, after use?</asp:Label>
                 </td>
             </tr>
             <tr>
                 <td class="auto-style12">
                     <asp:Label ID="Label21" runat="server" Text="3"></asp:Label>
                 </td>
                 <td class="auto-style7">
                     <asp:Label ID="Label25" runat="server">will driver be allocated with the car?</asp:Label>
                 </td>
             </tr>
             <tr>
                 <td class="auto-style12">
                     <asp:Label ID="Label22" runat="server" Text="4"></asp:Label>
                 </td>
                 <td class="auto-style7">
                     <asp:Label ID="Label26" runat="server">shall we take the car out of the state?</asp:Label>
                 </td>
             </tr>
             <tr>
                 <td class="auto-style12">&nbsp;</td>
                 <td class="auto-style7">&nbsp;</td>
             </tr>
         </table>
    </div>
    <div>
        <asp:Label ID="lblcontact" style="margin-left:100px" runat="server" Text="Contact Info:"></asp:Label>
        </div>
    <div>
         <table class="auto-style13">
             <tr>
                 <td class="auto-style14">&nbsp;</td>
                 <td>&nbsp;</td>
             </tr>
             <tr>
                 <td class="auto-style14">
                     <asp:Label ID="Label27" runat="server">Company Name</asp:Label>
                 </td>
                 <td>
                     <asp:Label ID="Label28" runat="server">Amman Car-Rental </asp:Label>
                 </td>
             </tr>
             <tr>
                 <td class="auto-style14">
                     <asp:Label ID="Label29" runat="server">Company Address</asp:Label>
                 </td>
                 <td>
                     <asp:Label ID="Label30" runat="server">20/1 ,New Busstand East,Kumbakonam</asp:Label>
                 </td>
             </tr>
             <tr>
                 <td class="auto-style14">
                     <asp:Label ID="Label31" runat="server">Phone no.</asp:Label>
                 </td>
                 <td>
                     <asp:Label ID="Label32" runat="server">04367224516</asp:Label>
                 </td>
             </tr>
             <tr>
                 <td class="auto-style14">&nbsp;</td>
                 <td>&nbsp;</td>
             </tr>
         </table>
    </div>
</asp:Content>
