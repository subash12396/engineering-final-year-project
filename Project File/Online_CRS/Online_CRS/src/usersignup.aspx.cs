﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace Online_CRS
{
    public partial class usersignup : System.Web.UI.Page
    {
        static string user_name, user_email, user_pass, user_conpass, user_add, user_phone;

        public userlogin userlogin
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public booking booking
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        protected void b_login_Click(object sender, EventArgs e)
        {
            Server.Transfer("userlogin.aspx");
        }

        protected void b_signup_Click(object sender, EventArgs e)
        {
            user_name = tb_name.Text.ToString();

            user_email = tb_emailid.Text.ToString();
            user_pass = tb_password.Text.ToString();
            user_conpass = tb_confirmpassword.Text.ToString();
            user_add = tb_address.Text.ToString();
            user_phone = tb_phoneno.Text.ToString();
            using (SqlConnection con = new SqlConnection())
            {

                con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                con.Open();
                SqlCommand com = new SqlCommand("insert userdetails values(@username,@user_emailid,@user_pass,@user_conpass,@user_add,@user_phone_no)", con);
                com.Parameters.AddWithValue("@username", user_name);
                com.Parameters.AddWithValue("@user_emailid", user_email);
                com.Parameters.AddWithValue("@user_pass", user_pass);
                com.Parameters.AddWithValue("@user_conpass", user_conpass);
                com.Parameters.AddWithValue("@user_add", user_add);
                com.Parameters.AddWithValue("@user_phone_no", user_phone);
                com.ExecuteNonQuery();
            }
            Session["emailid"] = user_email;
            Response.Redirect("Booking.aspx");
        }


    }
}