﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace Online_CRS
{
    public partial class userlogin : System.Web.UI.Page
    {
        static string email, pass;

        public usersignup usersignup
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public booking booking
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void b_login_Click(object sender, EventArgs e)
        {
            Label2.Visible = false;
            email = tb_emailid.Text.ToString();
            pass = tb_password.Text.ToString();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                conn.Open();
                SqlCommand comm = new SqlCommand("select user_pass from userdetails where user_emailid=@email and user_pass=@pass", conn);
                comm.Parameters.AddWithValue("@email", email);
                comm.Parameters.AddWithValue("@pass", pass);  
                SqlDataReader reader = comm.ExecuteReader();
                if (reader.Read())
                {
                    
                    Session["emailid"] = email;
                    Response.Redirect("Booking.aspx");

                }
                else
                {
           
                    Label2.Visible = true;
                    tb_emailid.Text = "";
                }
            }
        }

        protected void b_register_Click(object sender, EventArgs e)
        {

            Server.Transfer("usersignup.aspx");
        }
    }
}