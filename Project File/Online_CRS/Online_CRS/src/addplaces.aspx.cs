﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Online_CRS
{
    public partial class addplaces : System.Web.UI.Page
    {
        public string places1 = " ", places2 = " ", places3 = " ";
        public string more_places;
         

        public booking booking
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void b_done_Click(object sender, EventArgs e)
        {
            string selected_road = rbl_highway.SelectedValue;
            string selected_traffic = rbl_traffic.SelectedValue;
            int[] selected_int = new int[3];
            int[] selected_places;
            selected_int[1] = rbl_highway.SelectedIndex;
            selected_int[2] = rbl_traffic.SelectedIndex;
            Session["selected_int"] = selected_int;
            int i = 0;
            int numSelected = 0;
            foreach (ListItem li in cbl_addplace1.Items)
            {
                if (li.Selected)
                {
                    numSelected = numSelected + 1;
                }
            }
            foreach (ListItem li in cbl_addplace2.Items)
            {
                if (li.Selected)
                {
                    numSelected = numSelected + 1;
                }
            }
            foreach (ListItem li in cbl_addplace3.Items)
            {
                if (li.Selected)
                {
                    numSelected = numSelected + 1;
                }
            }
            selected_places = new int[numSelected+1];
            Session["count"] = numSelected;
            foreach (ListItem item in cbl_addplace1.Items)
            {
                if (item.Selected)
                {
                    
                    selected_places[i] = cbl_addplace1.Items.IndexOf(item);
                    i++;
                }
            }
           
            foreach (ListItem item in cbl_addplace2.Items)
            {
                if (item.Selected)
                {
                    selected_places[i] =  cbl_addplace2.Items.IndexOf(item) + 11;
                    i++;
                }
            }
            foreach (ListItem item in cbl_addplace3.Items)
            {
                if (item.Selected)
                {
                    selected_places[i] = cbl_addplace3.Items.IndexOf(item) + 21;
                    i++;
                }
            }
            Session["selected_places_id"] = selected_places;
           
           

            List<string> selectedValues1 = cbl_addplace1.Items.Cast<ListItem>()
               .Where(li => li.Selected)
                .Select(li => li.Value)
                .ToList();
            List<string> selectedValues2 = cbl_addplace2.Items.Cast<ListItem>()
             .Where(li => li.Selected)
              .Select(li => li.Value)
              .ToList();
            List<string> selectedValues3 = cbl_addplace3.Items.Cast<ListItem>()
             .Where(li => li.Selected)
              .Select(li => li.Value)
              .ToList();

            places1 = string.Join("  ", selectedValues1.ToArray());
            places2 = string.Join("  ", selectedValues2.ToArray());
            places3 = string.Join("  ", selectedValues3.ToArray());
            more_places = places1 + places2 + places3;
            Session["add_places"] = more_places;
        }
    }
}