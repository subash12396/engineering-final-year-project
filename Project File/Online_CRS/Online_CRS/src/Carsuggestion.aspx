﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site(user login signup).Master" AutoEventWireup="true" CodeBehind="Carsuggestion.aspx.cs" Inherits="Online_CRS.Carsuggestion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            margin-bottom: 117px;
        }
        .auto-style2 {
            width: 247px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
    <div>
    
        <asp:Label ID="lbl_suggestion" runat="server" Text="CAR SUGGESTION AND ROUTE SUGGESTION:" style="margin-left:200px"></asp:Label>

    </div>
        <div>

            <asp:Label ID="lbl_optional" runat="server" Text="Optional car based on your interest:"></asp:Label>

        </div>
        <div style="height: 180px">

            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="Label1" runat="server" Text="Route"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="tb_route" runat="server" Height="72px" TextMode="MultiLine" Width="371px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lbl_need" runat="server" Text="Need to select a car:"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="b_getcar" runat="server" OnClick="b_getcar_Click" Text="Get Cars" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>
                   
                        <asp:RadioButtonList ID="rbl_need" runat="server" AutoPostBack="True" Visible="False" Width="162px" Height="16px" OnSelectedIndexChanged="rbl_need_SelectedIndexChanged" >
                        </asp:RadioButtonList>
                        
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">

            <asp:Label ID="lbl_amount" runat="server" Text="Amount per day"></asp:Label>
                    </td>
                    <td>
            <asp:TextBox ID="tb_amount" runat="server" Height="19px" Width="145px"></asp:TextBox>

                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>

            
<asp:Button ID="bt_done" runat="server" Text="Done" style="margin-left:20px" OnClick="bt_done_Click" />
                    </td>
                </tr>
            </table>
            <br />
            <br />

        </div>
        <div>

&nbsp;
            
        </div>
        <div>

            
        </div>
        
</asp:Content>

