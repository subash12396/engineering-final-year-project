﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
namespace Online_CRS
{
    public partial class Settings : System.Web.UI.Page
    {
        string emailid;

        public Profile Profile
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["emailid"] != null)
                emailid = Session["emailid"].ToString();
        }

        protected void b_update_Click(object sender, EventArgs e)
        {
            string u_name, u_add, u_phone,u_pass,u_conpass;
            u_name = tb_name.Text.ToString();
            u_pass = tb_newpassword.Text.ToString();
            u_conpass = tb_confirmpassword.Text.ToString();
            u_add = tb_address.Text.ToString();
            u_phone = tb_phone.Text.ToString();
            using (SqlConnection con = new SqlConnection())
            {

                con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                con.Open();
                SqlCommand com = new SqlCommand("update userdetails set username=@name,user_pass=@pass,user_conpass=@cpass,user_add=@add,user_phone_no=@phone where user_emailid=@emailid", con);
                com.Parameters.AddWithValue("@name", u_name);
                com.Parameters.AddWithValue("@pass",u_pass);
                com.Parameters.AddWithValue("@cpass", u_conpass);
                com.Parameters.AddWithValue("@add", u_add);
                com.Parameters.AddWithValue("@phone", u_phone);
                com.Parameters.AddWithValue("@emailid", emailid);
                try
                {
                    com.ExecuteNonQuery();
                    string script = "alert('Your Details are updated successfully.');";
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
                }
                catch (Exception ex){
                   
                }
            }
        }

        protected void b_back_Click(object sender, EventArgs e)
        {
            Server.Transfer("profile.aspx");
        }
    }
}