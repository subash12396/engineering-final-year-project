﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="addplaces.aspx.cs" Inherits="Online_CRS.addplaces" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            margin-right: 0px;
            width: 464px;
        }
        .auto-style2 {
            width: 100%;
        }
        .auto-style5 {
            margin-left: 0px;
        }
        .auto-style6 {
            width: 232px;
        }
        .auto-style7 {
            width: 192px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
 <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
           
        <div>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="Label1" runat="server" Text="Select the Cities to visit "></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        <asp:CheckBoxList ID="cbl_addplace1" runat="server" AutoPostBack="True" Height="151px" Width="169px">
                            <asp:ListItem>Ariyalur</asp:ListItem>
                            <asp:ListItem>Chennai</asp:ListItem>
                            <asp:ListItem>Coimbatore</asp:ListItem>
                            <asp:ListItem>Cuddalore</asp:ListItem>
                            <asp:ListItem>Dharmapuri</asp:ListItem>
                            <asp:ListItem>Dindigul</asp:ListItem>
                            <asp:ListItem>Erode</asp:ListItem>
                            <asp:ListItem>Kanchipuram</asp:ListItem>
                            <asp:ListItem>Kanniyakumari</asp:ListItem>
                            <asp:ListItem>Karur</asp:ListItem>
                            <asp:ListItem>Krishnagiri</asp:ListItem>
                        </asp:CheckBoxList>
                    </td>
                    <td class="auto-style6">
                        <asp:CheckBoxList ID="cbl_addplace2" runat="server" AutoPostBack="True" Width="173px">
                            <asp:ListItem>Madurai</asp:ListItem>
                            <asp:ListItem>Nagapattinam</asp:ListItem>
                            <asp:ListItem>Namakkal</asp:ListItem>
                            <asp:ListItem>Perambalur</asp:ListItem>
                            <asp:ListItem>Pudukkottai</asp:ListItem>
                            <asp:ListItem>Ramanathapuram</asp:ListItem>
                            <asp:ListItem>Salem</asp:ListItem>
                            <asp:ListItem>Sivaganga</asp:ListItem>
                            <asp:ListItem>Thanjavur</asp:ListItem>
                            <asp:ListItem>Theni</asp:ListItem>
                        </asp:CheckBoxList>
                    </td>
                    <td>
                        <asp:CheckBoxList ID="cbl_addplace3" runat="server" AutoPostBack="True" Width="168px">
                            <asp:ListItem>Thoothukudi</asp:ListItem>
                            <asp:ListItem>Tiruchirappalli</asp:ListItem>
                            <asp:ListItem>Tirunelveli</asp:ListItem>
                            <asp:ListItem>Tiruppur</asp:ListItem>
                            <asp:ListItem>Tiruvallur</asp:ListItem>
                            <asp:ListItem>Tiruvannamalai</asp:ListItem>
                            <asp:ListItem>Tiruvarur</asp:ListItem>
                            <asp:ListItem>Vellore</asp:ListItem>
                            <asp:ListItem>Viluppuram</asp:ListItem>
                            <asp:ListItem>Virudhunagar</asp:ListItem>
                        </asp:CheckBoxList>
                    </td>
                </tr>
            </table>

        </div>
        <div>

            <table class="auto-style2">
                <tr>
                    <td class="auto-style7">
                        <asp:RadioButtonList ID="rbl_highway" runat="server" AutoPostBack="True">
                            <asp:ListItem>State Highway</asp:ListItem>
                            <asp:ListItem>National Highway</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="rbl_highway" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rbl_traffic" runat="server" AutoPostBack="True">
                            <asp:ListItem>With Traffic</asp:ListItem>
                            <asp:ListItem>Without Traffic</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="rbl_traffic" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>

                <tr>
                    <td class="auto-style7">&nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>

        </div>
        <div>
            

            <asp:Button ID="b_done" runat="server" style="margin-left:400px; height: 26px;" Text="Done" OnClick="b_done_Click" />
  
        </div>
       
    </form>
</body>
</html>
