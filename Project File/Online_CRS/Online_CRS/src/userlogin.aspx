﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site(user login signup).Master" AutoEventWireup="true" CodeBehind="userlogin.aspx.cs" Inherits="Online_CRS.userlogin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 196px;
        }
        .auto-style2 {
            width: 671px;
            height: 139px;
        }
        .auto-style3 {
            height: 31px;
            width: 375px;
        }
        .auto-style4 {
            width: 196px;
            height: 31px;
        }
        .auto-style5 {
            width: 375px;
        }
        .auto-style6 {
            width: 375px;
            height: 47px;
        }
        .auto-style7 {
            width: 196px;
            height: 47px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table class="auto-style2">
            <tr>
                <td class="auto-style5">
                    <asp:Label ID="lblemail" style="margin-left:310px" runat="server" Text="Email Id"></asp:Label>
                </td>
                <td class="auto-style1">
                    <asp:TextBox ID="tb_emailid" runat="server" Width="180px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tb_emailid" ErrorMessage="*" ForeColor="#000099"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">
                    <asp:Label ID="lblpassword" style="margin-left:300px" runat="server" Text="Password"></asp:Label>
                </td>
                <td class="auto-style4">
                    <asp:TextBox ID="tb_password" runat="server" TextMode="Password" Width="180px" Height="17px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tb_password" ErrorMessage="*" ForeColor="#000099"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style6"></td>
                <td class="auto-style7">
                    <asp:Button ID="b_login1" runat="server" Text="Login" OnClick="b_login_Click" />
                </td>
            </tr>
            
           
            <tr>
                <td class="auto-style5">&nbsp;</td>
                <td class="auto-style1">
                    <asp:Label ID="Label2" runat="server" Text="invalid Email ID or Password" Visible="False"></asp:Label>
                    </td>
            </tr>
            
           
           </table>
        </div>
    <div>

        <asp:Label ID="Label1" style="margin-left:400px" runat="server" Text="------------OR-----------"></asp:Label>

    </div>
    <div>

        <asp:Button ID="b_register" style="margin-left:440px" runat="server" Text="Register" CausesValidation="False" OnClick="b_register_Click" />

    </div>
</asp:Content>
