﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site(user login signup).Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="Online_CRS.Profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 33px;
        }
        .auto-style2 {
            height: 34px;
        }
        .auto-style3 {
            height: 37px;
        }
        .auto-style4 {
            height: 28px;
        }
        .auto-style5 {
            height: 33px;
            width: 569px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
     <div>
    
        <asp:Label ID="Label1" Style="margin-left:300px" runat="server" Text="Profile"></asp:Label>
    
    </div>
    <div>

            </div>
        <div>

            <table class="auto-style5">
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lbl_name" runat="server" Text="Name"></asp:Label>
                    </td>
                    <td class="auto-style3">
                        <asp:TextBox ID="tb_name" runat="server" Width="180px"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lbl_emailid" runat="server" Text="Email ID"></asp:Label>
                    </td>
                    <td class="auto-style3">
                        <asp:TextBox ID="tb_emailid" runat="server" Width="180px"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lbl_address" runat="server" Text="Address"></asp:Label>
                    </td>
                    <td class="auto-style3">
                        <asp:TextBox ID="tb_address" runat="server" TextMode="MultiLine" Width="180px"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lbl_phone" runat="server" Text="Phone"></asp:Label>
                    </td>
                    <td class="auto-style3">
                        <asp:TextBox ID="tb_phone" runat="server" Width="180px"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Button ID="bt_myreservation" runat="server" OnClick="bt_myreservation_Click" Text="My Reservation" />
                    </td>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style4"></td>
                    <td class="auto-style4">

            <asp:Button ID="bt_settings" runat="server" Text="Settings" s OnClick="bt_settings_Click"/>

                    </td>
                    <td class="auto-style4"></td>
                </tr>
                <tr>
                    <td class="auto-style1"></td>
                    <td class="auto-style1">
                        <asp:Button ID="bt_okay" runat="server" Text="Okay" OnClick="bt_okay_Click" />
                    </td>
                    <td class="auto-style1"></td>
                </tr>
            </table>

            </div>
</asp:Content>
