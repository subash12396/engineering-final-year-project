﻿    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Data.SqlClient;
    using System.Runtime.InteropServices;
    using System.Web.SessionState;
    using Excel = Microsoft.Office.Interop.Excel;

    namespace Online_CRS
    {
        public partial class booking : System.Web.UI.Page
        {
        static int[,] n_dist = new int[16, 16] {{-1,303,313,126,217,185,214,260,462,138,268,218,121,129,33,244},
          {303,-1,520,201,317,440,421,85,718,400,272,474,330,402,288,501},
          {313,520,-1,367,227,152,101,462,442,131,277,213,360,154,265,228},
          {126,201,367,-1,269,285,265,147,563,252,216,319,131,247,133,344},
          {217,317,227,269,-1,234,125,239,525,157,55,295,337,114,182,287},
          {185,440,152,285,234,-1,134,385,294,86,286,65,243,124,156,58},
          {214,421,101,265,125,134,-1,361,424,66,177,195,295,58,180,187},
          {260,85,462,147,239,385,361,-1,664,392,191,420,276,326,234,446},
          {462,718,442,563,525,294,424,664,-1,373,579,246,522,416,436,255},
          {138,400,131,252,157,86,66,392,373,-1,210,242,225,47,130,133},
          {268,272,277,216,55,286,177,191,579,210,-1,345,386,163,231,336},
          {218,474,213,319,295,65,195,420,246,142,345,-1,277,186,191,112},
          {121,330,360,131,337,243,295,276,522,225,386,277,-1,273,197,149},
          {129,402,154,247,114,124,58,326,416,47,163,186,273,-1,96,146},
          {33,288,265,133,182,156,180,234,436,130,231,191,197,96,-1,111},
          {244,501,228,344,287,58,187,446,255,133,336,112,149,146,111,-1}};

        static string[,] n_no = new string[16, 16] {{"N/A","132","544","81","79","83","544","132","136","81","44","38","S65","S161","136","83"},
          {"132","N/A","79","32","AH45","38","79","48","38","81","AH45","38","32","79","38","132"},
          {"544","79","N/A","544","544","S37","544","45","44","81","544","S37","81","81","81","44"},
          {"81","32","544","N/A","79","83","79","32","38","36","77","38","32","79","38","83"},
          {"79","AH45","544","79","N/A","44","544","AH45","44","44","44","44","532","44","79","44"},
          {"83","38","S37","83","44","N/A","S84","38","44","44","44","44","83","44","83","183"},
          {"544","79","544","79","544","S84","N/A","AH45","44","S84","544","44","81","S79","79","183"},
          {"132","48","45","32","AH45","38","AH45","N/A","38","AH45","48","38","32","132","38","38"},
          {"136","38","44","38","44","44","44","38","N/A","44","44","44","38","44","38","744"},
          {"81","81","81","36","44","44","S84","AH45","44","N/A","44","44","83","44","81","44"},
          {"44","AH45","544","77","44","44","544","48","44","44","N/A","44","79","44","79","44"},
          {"38","38","S37","38","44","44","44","38","44","44","44","N/A","35","44","38","36"},
          {"S65","32","81","32","532","83","81","32","38","53","79","35","N/A","81","83","83"},
          {"S161","79","81","79","44","44","S79","132","44","44","44","44","81","N/A","S161","81"},
           {"136","38","81","38","79","83","79","38","38","89","79","38","83","S161","N/A","336"},
          {"83","132","44","83","44","183","183","38","144","44","44","36","83","81","336","N/A"}};

        static string[,] n_traffic = new string[16, 16] {{"N/A","132","544","38","79","83","544","132","136","38","44","38","S65","S25","136","83"},
          {"132","N/A","79","32","AH45","38","79","48","38","79","AH45","38","32","79","38","132"},
          {"544","38","N/A","544","544","S37","544","45","44","544","544","S37","81","81","544","44"},
          {"38","32","544","N/A","79","83","79","32","38","79","77","38","32","79","38","83"},
          {"79","AH45","544","79","N/A","44","544","AH45","44","44","44","44","532","44","79","44"},
          {"83","38","S37","83","44","N/A","S84","38","44","44","44","44","83","44","83","183"},
          {"544","79","544","79","544","S84","N/A","AH45","44","S84","544","44","83","S79","79","183"},
          {"132","48","45","32","AH45","38","AH45","N/A","38","AH45","48","38","32","AH45","38","38"},
          {"136","38","44","38","44","44","44","38","N/A","44","44","44","38","44","38","44"},
          {"38","79","544","79","44","44","S84","AH45","44","N/A","44","44","83","44","81","44"},
          {"44","AH45","544","77","44","44","544","38","44","44","N/A","44","79","44","79","44"},
          {"38","38","S37","38","44","44","44","38","44","44","44","N/A","35","44","38","36"},
          {"S65","32","81","32","532","83","83","32","38","83","79","35","N/A","81","83","83"},
          {"S25","79","81","79","44","44","S79","AH45","44","44","44","44","81","N/A","S161","81"},
          {"136","38","544","38","79","83","79","38","38","89","79","38","83","S161","N/A","336"},
          {"83","132","44","83","44","183","183","38","44","44","44","36","83","81","336","N/A"}};

        static int[,] s_dist = new int[16, 16] {{-1,337,288,135,217,210,187,287,467,138,270,218,121,129,33,244},
          {337,-1,520,175,309,440,421,85,718,400,272,474,306,402,288,501},
          {288,520,-1,367,209,152,101,423,442,131,260,213,360,154,249,193},
          {135,175,367,-1,224,285,269,147,563,252,220,319,146,225,133,344},
          {217,309,209,224,-1,234,125,235,525,157,62,295,337,114,206,319},
          {210,440,152,285,234,-1,134,385,294,84,286,65,271,144,156,58},
          {187,421,101,269,125,134,-1,323,424,66,177,210,335,58,154,233},
          {287,85,423,147,235,385,323,-1,664,392,191,420,344,326,234,446},
          {467,718,442,563,525,294,424,664,-1,373,579,246,475,416,436,255},
          {138,400,131,252,157,84,66,392,373,-1,210,142,225,41,142,133},
          {270,272,260,220,62,286,177,191,579,210,-1,345,386,163,79,336},
          {218,474,213,319,295,65,210,420,246,142,345,-1,253,191,191,112},
          {121,306,360,146,337,271,335,344,475,225,386,253,-1,226,155,162},
          {129,402,154,225,114,144,58,326,416,41,163,191,226,-1,96,142},
          {33,288,249,133,206,156,154,234,436,142,79,191,155,96,-1,111},
          {244,501,193,344,319,58,233,446,255,133,336,112,162,142,111,-1}};

        static string[,] s_no = new string[16, 16] {{"N/A","5","25","140","N79","71","161","137","N136","N81","9","N38","65","161","N136","N83"},
          {"5","N/A","N79","49","133","N38","N79","48","N38","N81","AH45","N38","49","N79","N38","N132"},
          {"25","N79","N/A","N544","82","37","N544","6A","N44","189","20","37","N81","N81","189","37"},
          {"140","49","N544","N/A","6A","N83","N79","N32","N38","N36","9","N38","67","142","N38","N83"},
          {"N79","133","22","6A","N/A","N44","N544","60","N44","N44","17","N44","N532","N44","30","37"},
          {"71","N38","37","N83","N44","N/A","84","N38","N44","74","N44","N44","71","71","N83","N183"},
          {"161","N79","N544","N79","N544","84","N/A","6A","N44","84","N544","37","65","79","161","37"},
          {"137","48","68","N32","60","N38","6A","N/A","N38","AH45","N48","N38","65","N132","N38","N38"},
          {"N136","N38","N44","N38","N44","N44","N44","N38","N/A","N44","N44","N44","49","N44","N38","N744"},
          {"N81","N81","189","N36","N44","74","84","AH45","N44","N/A","N44","N44","N53","547","161","N44"},
          {"9","AH45","20","9","17","N44","N544","N48","N44","N44","N/A","N44","N79","N44","N79","N44"},
          {"N38","N38","37","N38","N44","N44","37","N38","N44","N44","N44","N/A","49","71","N38","N36"},
          {"65","49","N81","67","N532","71","65","65","49","N83","N79","49","N/A","22","65","71"},
          {"161","N79","N81","142","N44","71","79","N132","N44","547","N44","71","22","N/A","161","71"},
          {"N136","N38","189","N38","30","N83","161","N38","N38","161","N79","N38","65","161","N/A","N336"},
          {"N83","N132","37","N83","37","N183","37","N38","N744","N44","N44","N36","71","71","N336","N/A"}};

        static string[,] s_low = new string[16, 16] {{"N/A","5","25","140","N79","71","84","137","N136","N38","9","N38","65","25","N136","N83"},
          {"5","N/A","N79","49","133","N38","N79","48","N38","N79","AH45","N38","49","N79","N38","N132"},
          {"25","N79","N/A","N544","82","37","N544","6A","N44","189","544","37","N81","N81","189","37"},
          {"140","49","N544","N/A","863","N83","N79","N32","N38","N79","9","N38","67","25","N38","N83"},
          {"N79","133","82","863","N/A","N44","N544","60","N44","N44","17","N44","N532","N/A","30","37"},
          {"71","N38","37","N83","N44","N/A","84","N38","N44","74","N44","N44","71","71","N83","N183"},
          {"84","N79","N544","N79","N544","84","N/A","6A","N44","84","N544","37","65","79","161","37"},
          {"137","48","6A","N32","60","N38","6A","N/A","N38","AH45","N48","N38","65","AH45","N38","N38"},
          {"N136","N38","N44","N38","N44","74","84","AH45","N/A","N44","N44","N44","49","N44","N38","N44"},
          {"N38","N79","189","N79","N44","74","84","AH45","N74","N/A","N44","N44","N83","547","161","N44"},
          {"9","AH45","544","9","17","N44","N544","N48","N44","N44","N/A","N44","N79","N44","N79","N44"},
          {"N38","N38","37","N38","N44","N44","37","N38","N44","N44","N44","N/A","49","71","N38","N36"},
          {"65","49","N81","67","N532","71","65","65","49","N83","N79","49","N/A","22","65","71"},
          {"25","N79","N81","25","N44","71","79","AH45","N44","547","N41","71","22","N/A","161","71"},
          {"N136","N38","189","N38","30","N83","161","N38","N44","161","N79","N38","65","161","N/A","N336"},
          {"N83","N132","37","N83","37","N183","37","N38","N44","N44","N44","N36","71","71","N336","N/A"}};
        static string pickup_loc, emailid, adding_places, pickup_date, drop_date, drop_loc, pickup_time, drop_time, driver_req, driver_gender, driver_exp, selected_car;
            static double amount;
            public string places1 = " ", places2 = " ", places3 = " ";
            public string more_places;
            static double[] price;
            int[] select_car_id;
            string[] car_name;
            int[] selected_int;
            int[] selected_places;
        static string result_route;

        int i = 1;

            public addplaces addplaces
            {
                get
                {
                    throw new System.NotImplementedException();
                }

                set
                {
                }
            }

            public Carsuggestion Carsuggestion
            {
                get
                {
                    throw new System.NotImplementedException();
                }

                set
                {
                }
            }

            public Profile Profile
            {
                get
                {
                    throw new System.NotImplementedException();
                }

                set
                {
                }
            }

            protected void Page_Load(object sender, EventArgs e)
            {

            
            }
            protected void b_done1_Click1(object sender, EventArgs e)
            {
                pickup_loc = ddl_pickuplocation.SelectedItem.Text;
                drop_loc = ddl_droplocation.SelectedItem.Text;
                int start = ddl_pickuplocation.SelectedIndex;
                int end = ddl_droplocation.SelectedIndex;
                pickup_time = ((TextBox)tb_pickuptime).Text.ToString();
                drop_time = ((TextBox)tb_droptime).Text.ToString();
                pickup_date = ((TextBox)tb_pickupdate).Text.ToString();
                drop_date = ((TextBox)tb_dropdate).Text.ToString();
                int[] result;
                int[] obj;
                int car_id;
                int order_id;
                
                
                if (rb_driverreqyes.Checked)
                {
                    driver_req = "yes";
                    if (rb_lady.Checked)
                        driver_gender = "Lady";
                    else if (rb_gents.Checked)
                        driver_gender = "Gent";
                    if (rb_5years.Checked)
                        driver_exp = "5 Years +";
                    else if (rb_8years.Checked)
                        driver_exp = "8 Years +";
                    else if (rb_14years.Checked)
                        driver_exp = "14 Years +";
                }
                else if (rb_driverreqno.Checked)
                {
                    driver_req = "no";
                    driver_gender = "-";
                    driver_exp = "-";
                }
            int count=2;
            if (Session["emailid"] != null)
                    emailid = Session["emailid"].ToString();
            adding_places = " ";
            if (Session["add_places"] != null)
                adding_places = Session["add_places"].ToString();

            if (Session["selected_places_id"] != null)
                    selected_places = Session["selected_places_id"] as int[];

            if (Session["selected_int"] != null)
                    selected_int= Session["selected_int"] as int[];
            if (Session["count"] != null)
                count = Convert.ToInt32(Session["count"]);

                int numSelected = 0;
                foreach (ListItem li in cbl_parameters.Items)
                {
                    if (li.Selected)
                    {
                        numSelected = numSelected + 1;
                    }
                }
                obj = new int[numSelected + 1];
                foreach (ListItem item in cbl_parameters.Items)
                {
                    if (item.Selected)
                    {
                        obj[i] = cbl_parameters.Items.IndexOf(item) + 1;
                        i++;
                    }

                }
           
                result=forming_array1(obj);
                string apps = " ";
                int j;
                Session["car_name"] =car_name;
                Session["Car_id"] = select_car_id;
                price = new double[car_name.Length + 1];
                for (i = 1; i < select_car_id.Length; i++)
                {
               
                    using (SqlConnection con = new SqlConnection())
                    {

                        con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                        con.Open();
                        SqlCommand com = new SqlCommand("select price from car_details where car_id=@carid", con);
                        com.Parameters.AddWithValue("@carid", select_car_id[i]);
                        SqlDataReader nwReader = com.ExecuteReader();
                        while (nwReader.Read())
                        {
                            price[i] = Convert.ToDouble((nwReader["price"]));
                        }
                        nwReader.Close();
                        com.ExecuteNonQuery();
                        nwReader = null;
                    }
                    
                }
                apps = " " + start + " " + end;
                for (i = 0; i < selected_places.Length - 1; i++)
                {
                    apps = apps + " " + selected_places[i];
                }
                apps = apps + " " + selected_int[1] + " " + selected_int[2];
                tb_pickuplocation.Text = apps.ToString();
                Session["Price"] = price;
                selected_car = "-";
                car_id = 0;
                amount = 0;

                using (SqlConnection con = new SqlConnection())
                {

                    con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                    con.Open();
                    SqlCommand com = new SqlCommand("insert orderdetails values(@user_emailid,@pickup_location,@drop_loaction,@places_to_visit,@pickup_date,@pickup_time,@drop_time,@drop_date,@driver_req,@driver_type,@driver_experience,@selected_car,@car_id,@amount)", con);
                    com.Parameters.AddWithValue("@user_emailid", emailid);
                    com.Parameters.AddWithValue("@pickup_location", pickup_loc);
                    com.Parameters.AddWithValue("@drop_loaction", drop_loc);
                    com.Parameters.AddWithValue("@places_to_visit", adding_places);
                    com.Parameters.AddWithValue("@pickup_date", pickup_date);
                    com.Parameters.AddWithValue("@pickup_time", pickup_time);
                    com.Parameters.AddWithValue("@drop_time", drop_time);
                    com.Parameters.AddWithValue("@drop_date", drop_date);
                    com.Parameters.AddWithValue("@driver_req", driver_req);
                    com.Parameters.AddWithValue("@driver_type", driver_gender);
                    com.Parameters.AddWithValue("@driver_experience", driver_exp);
                    com.Parameters.AddWithValue("@selected_car", selected_car);
                    com.Parameters.AddWithValue("@car_id", car_id);
                    com.Parameters.AddWithValue("@amount", amount);
                    com.ExecuteNonQuery();
                }
                using (SqlConnection con = new SqlConnection())
                {

                    con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                    con.Open();
                    SqlCommand com = new SqlCommand("select order_id from orderdetails where user_emailid=@user_emailid and pickup_location=@pickup_location and drop_location =@drop_location and pickup_date=@pickup_date and pickup_time=@pickup_time and drop_time=@drop_time and drop_date=@drop_date", con);
                    com.Parameters.AddWithValue("@user_emailid", emailid);
                    com.Parameters.AddWithValue("@pickup_location", pickup_loc);
                    com.Parameters.AddWithValue("@drop_location", drop_loc);
                    com.Parameters.AddWithValue("@pickup_date", pickup_date);
                    com.Parameters.AddWithValue("@pickup_time", pickup_time);
                    com.Parameters.AddWithValue("@drop_time", drop_time);
                    com.Parameters.AddWithValue("@drop_date", drop_date);
                    order_id = Convert.ToInt32(com.ExecuteScalar());
                    com.ExecuteNonQuery();
                }
          result_route= Optimal_routing(start,end,selected_places,selected_places.Length-1,selected_int[1],selected_int[2]);
                Session["order_id"] = order_id;
            Session["route"] = result_route;
            tb_droplocation.Text = result_route.ToString();
            Server.Transfer("Carsuggestion.aspx"); 
           
            }
            public string Optimal_routing(int start,int end,int[] inter,int n,int choice,int t)
            {
                int note = 0;
                int s, i, j,low, r_i = 0, check = 0;
                int[] route = new int[30];
                string result_route=" ";
                string[] rr = new string[16] { "ARIYALUR", "CHENNAI", "COIMBATORE", "CUDDALORE", "DHARMAPURI", "DINDIGUL", "ERODE", "KANCHEEPURAM", "KANYAKUMARI", "KARUR", "KRISHNAGIRI", "MADURAI", "NAGAPATTINAM", "NAMAKKAL", "PERAMBALUR", "PUDUKOTTAI" };
                if (choice == 0 && t == 0)
                {
                    s = start;
                    for (int k = 0; k < n; k++)
                    {
                        low = 5000;
                        for (i = 0; i < n; i++)
                        {
                            if (inter[i] != -1)
                            {
                                if ((n_dist[s, inter[i]]) < low && (n_dist[s, inter[i]]) != -1)
                                {
                                    low = n_dist[s, inter[i]];
                                    note = i;
                                }
                            }
                        }
                        route[r_i] = inter[note]; r_i++;
                        s = inter[note];
                        inter[note] = -1;
                    }
                    route[r_i] = end;
                    s = start;
                    result_route = result_route + " " + rr[start];
                    //Console.WriteLine(rr[start]);
                    for (i = 0; i <= r_i; i++)
                    {
                        result_route = result_route + " " + rr[route[i]];
                    }
                //Console.WriteLine("the route is");
                result_route = result_route + "\n";
                    for (i = 0; i <= r_i; i++)
                    {
                    result_route = result_route + " " + n_no[s, route[i]];
                       // Console.WriteLine(n_no[s, route[i]]);
                        s = route[i];
                    }
               
                }
                if (choice == 0 && t == 1)
                {
                    s = start;
                    for (int k = 0; k < n; k++)
                    {
                        low = 5000;
                        for (i = 0; i < n; i++)
                        {
                            if (inter[i] != -1)
                            {
                                if ((n_dist[s, inter[i]]) < low && (n_dist[s, inter[i]]) != -1)
                                {
                                    low = n_dist[s, inter[i]];
                                    note = i;
                                }
                            }
                        }
                        route[r_i] = inter[note]; r_i++;
                        s = inter[note];
                        inter[note] = -1;
                    }
                    route[r_i] = end;
                    s = start;
                result_route = result_route + " " + rr[start];
                //Console.WriteLine(rr[start]);
                for (i = 0; i <= r_i; i++)
                {
                    result_route = result_route + " " + rr[route[i]];
                }
                //Console.WriteLine("the route is");
                result_route = result_route + "\n";
                for (i = 0; i <= r_i; i++)
                    {
                    result_route = result_route + " " + n_traffic[s, route[i]];
                       // Console.WriteLine(n_traffic[s, route[i]]);
                        s = route[i];
                    }
                }
                if (choice == 1 && t == 0)
                {
                    s = start;
                    for (int k = 0; k < n; k++)
                    {
                        low = 5000;
                        for (i = 0; i < n; i++)
                        {
                            if (inter[i] != -1)
                            {
                                if ((s_dist[s, inter[i]]) < low && (s_dist[s, inter[i]]) != -1)
                                {
                                    low = s_dist[s, inter[i]];
                                    note = i;
                                }
                            }
                        }
                        route[r_i] = inter[note]; r_i++;
                        s = inter[note];
                        inter[note] = -1;
                    }
                    route[r_i] = end;
                    s = start;
                result_route = result_route + " " + rr[start];
                //Console.WriteLine(rr[start]);
                for (i = 0; i <= r_i; i++)
                {
                    result_route = result_route + " " + rr[route[i]];
                }
                //Console.WriteLine("the route is");
                result_route = result_route + "\n";
                for (i = 0; i <= r_i; i++)
                    {
                    result_route = result_route + " " + s_no[s, route[i]];
                     //   Console.WriteLine(s_no[s, route[i]]);
                        s = route[i];
                    }
                }
                if (choice == 1 && t == 1)
                {

                    s = start;
                    for (int k = 0; k < n; k++)
                    {
                        low = 5000;
                        for (i = 0; i < n; i++)
                        {
                            if (inter[i] != -1)
                            {
                                if ((s_dist[s, inter[i]]) < low && (s_dist[s, inter[i]]) != -1)
                                {
                                    low = s_dist[s, inter[i]];
                                    note = i;
                                }
                            }
                        }
                        route[r_i] = inter[note]; r_i++;
                        s = inter[note];
                        inter[note] = -1;

                    }
                    route[r_i] = end;
                    s = start;
                result_route = result_route + " " + rr[start];
                //Console.WriteLine(rr[start]);
                for (i = 0; i <= r_i; i++)
                {
                    result_route = result_route + " " + rr[route[i]];
                }
                //Console.WriteLine("the route is");
                result_route = result_route + "\n";
                for (i = 0; i <= r_i; i++)
                    {
                    result_route = result_route + " " + s_low[s, route[i]];
                        //Console.WriteLine(s_low[s, route[i]]);
                        s = route[i];
                    }
                }
            return result_route;
            }
            public int[] forming_array1(int[] obj)
            {
                int i=1, j=1;
                int[] result;
                int rowcount=0,colcount=14;
                int[] car_id;
                double[] t_price;
                double total_sum=0,avg;
                using (SqlConnection con = new SqlConnection())
                {

                    con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                    con.Open();
                    SqlCommand com = new SqlCommand("select count(*) from car_details",con);
                    rowcount = Convert.ToInt32(com.ExecuteScalar().ToString());
                     com.ExecuteNonQuery();
                }
                double[,] input = new double[colcount +4, rowcount + 4];
                string[] carname = new string[rowcount +4];
                car_id = new int[rowcount + 4];
                t_price = new double[rowcount + 4];
                using (SqlConnection con = new SqlConnection())
                {
                    con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                    con.Open();
                    SqlCommand com = new SqlCommand("select car_name from car_details  ", con);
                    SqlDataReader nwReader = com.ExecuteReader();
                    while (nwReader.Read())
                    {

                        carname[i] = nwReader["car_name"].ToString();
                        i++;
                    }
                    nwReader.Close();
                    com.ExecuteNonQuery();
                }
                j = 1;
                using (SqlConnection con = new SqlConnection())
                {
                    con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                    con.Open();
                    SqlCommand com = new SqlCommand("select car_id from car_details  ", con);
                    SqlDataReader nwReader = com.ExecuteReader();
                    while (nwReader.Read())
                    {

                        car_id[j]= Convert.ToInt32((nwReader["car_id"]));
                        j++;
                    }
                    nwReader.Close();
                    com.ExecuteNonQuery();
                }
                i = 1;j = 1;
                using (SqlConnection con = new SqlConnection())
                {
                    con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                    con.Open();
                    SqlCommand com = new SqlCommand("select comfort from car_details  ", con);
                    SqlDataReader nwReader = com.ExecuteReader();
                    while (nwReader.Read())
                    {

                        input[i, j] = Convert.ToDouble((nwReader["comfort"]));
                        j++;
                    }
                    nwReader.Close();
                    com.ExecuteNonQuery();
                }
                i++;
                j = 1;
                using (SqlConnection con = new SqlConnection())
                {
                    con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                    con.Open();
                    SqlCommand com = new SqlCommand("select safeties from car_details  ", con);
                    SqlDataReader nwReader = com.ExecuteReader();
                    while (nwReader.Read())
                    {

                        input[i, j] = Convert.ToDouble((nwReader["safeties"]));
                        j++;
                    }
                    nwReader.Close();
                    com.ExecuteNonQuery();
                }
                i++;
                j = 1;
                using (SqlConnection con = new SqlConnection())
                {
                    con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                    con.Open();
                    SqlCommand com = new SqlCommand("select quality from car_details  ", con);
                    SqlDataReader nwReader = com.ExecuteReader();
                    while (nwReader.Read())
                    {

                        input[i, j] = Convert.ToDouble((nwReader["quality"]));
                        j++;
                    }
                    nwReader.Close();
                    com.ExecuteNonQuery();
                }
                i++;
                j = 1;
                using (SqlConnection con = new SqlConnection())
                {
                    con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                    con.Open();
                    SqlCommand com = new SqlCommand("select media_feature from car_details  ", con);
                    SqlDataReader nwReader = com.ExecuteReader();
                    while (nwReader.Read())
                    {

                        input[i, j] = Convert.ToDouble((nwReader["media_feature"]));
                        j++;
                    }
                    nwReader.Close();
                    com.ExecuteNonQuery();
                }
                i++;
                j = 1;
                using (SqlConnection con = new SqlConnection())
                {
                    con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                    con.Open();
                    SqlCommand com = new SqlCommand("select city_mileage from car_details  ", con);
                    SqlDataReader nwReader = com.ExecuteReader();
                    while (nwReader.Read())
                    {

                        input[i, j] = Convert.ToDouble((nwReader["city_mileage"]));
                        j++;
                    }
                    nwReader.Close();
                    com.ExecuteNonQuery();
                }
                i++;
                j = 1;
                using (SqlConnection con = new SqlConnection())
                {
                    con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                    con.Open();
                    SqlCommand com = new SqlCommand("select high_mileage from car_details  ", con);
                    SqlDataReader nwReader = com.ExecuteReader();
                    while (nwReader.Read())
                    {

                        input[i, j] = Convert.ToDouble((nwReader["high_mileage"]));
                        j++;
                    }
                    nwReader.Close();
                    com.ExecuteNonQuery();
                }
                i++;
                j = 1;
                using (SqlConnection con = new SqlConnection())
                {
                    con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                    con.Open();
                    SqlCommand com = new SqlCommand("select cargo_volume from car_details  ", con);
                    SqlDataReader nwReader = com.ExecuteReader();
                    while (nwReader.Read())
                    {

                        input[i, j] = Convert.ToDouble((nwReader["cargo_volume"]));
                        j++;
                    }
                    nwReader.Close();
                    com.ExecuteNonQuery();
                }
                i++;
                j = 1;
                using (SqlConnection con = new SqlConnection())
                {
                    con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                    con.Open();
                    SqlCommand com = new SqlCommand("select seating_capacity from car_details  ", con);
                    SqlDataReader nwReader = com.ExecuteReader();
                    while (nwReader.Read())
                    {

                        input[i, j] = Convert.ToDouble((nwReader["seating_capacity"]));
                        j++;
                    }
                    nwReader.Close();
                    com.ExecuteNonQuery();
                }
                i++;
                j = 1;
                using (SqlConnection con = new SqlConnection())
                {
                    con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                    con.Open();
                    SqlCommand com = new SqlCommand("select speed from car_details  ", con);
                    SqlDataReader nwReader = com.ExecuteReader();
                    while (nwReader.Read())
                    {

                        input[i,j] = Convert.ToDouble((nwReader["speed"]));
                        j++;
                    }
                    nwReader.Close();
                    com.ExecuteNonQuery();
                }
                i++;
                j = 1;
                using (SqlConnection con = new SqlConnection())
                {
                    con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                    con.Open();
                    SqlCommand com = new SqlCommand("select price from car_details  ", con);
                    SqlDataReader nwReader = com.ExecuteReader();
                    while (nwReader.Read())
                    {

                        input[i,j] = Convert.ToDouble((nwReader["price"]));
                        j++;
                    }
                    nwReader.Close();
                    com.ExecuteNonQuery();
                }
          
                for(j=1;j<=rowcount;j++)
                {
                    total_sum = total_sum + input[i, j];              
                }
                avg = total_sum / (rowcount / 5);
                for(j=1;j<t_price.Length;j++)
                {
                    input[i, j] = avg - input[i,j];
                }
                string apps = " ";
                for (j = 1; j <= 10; j++)
                {
                    for (i = 1; i <= rowcount; i++)
                    {
                        apps = apps + "," + input[j, i];
                    }
                    apps = apps + "\n";
                }
                tb_droplocation.Text = apps.ToString();
                result = cnstDomMat(input, 10, rowcount, obj); //Algorithm 2
                car_name = new string[result.Length + 1];
                for (i = 1; i < result.Length; i++)
                {
                    car_name[i] = carname[result[i]];
                }
                select_car_id = new int[result.Length + 1];
                for (i = 1; i < result.Length; i++)
                {
                    select_car_id[i] =car_id[result[i]];
                }
                return (result);

            }
        
            //Algorithm 3
            public int[] dda_ns(int[,] d, int m, int n, int obj_len)
            {
                int i, j, l = 1;
                int count = 0;
                int[] result;
                int[] max = new int[100];
                int k = 1;
                int[] k1 = new int[100];
                int[] Q = new int[100];
                int[,] front = new int[100, 100];
                for (i = 1; i <= n; i++)
                {
                    for (j = 1; j <= n; j++)
                    {
                        if (d[i, j] == obj_len && d[j, i] == obj_len)
                        {
                            d[i, j] = 0;
                            d[j, i] = 0;
                        }
                    }
                }
                while (true)
                {
                    int m1;
                    l = 1;
                    int c = 0;
                    Q = new int[100];
                    for (i = 1; i <= n; i++)
                    {
                        m1 = 0;
                        c = 0;
                        for (j = 1; j <= n; j++)
                        {
                            if (m1 <= d[j, i])
                            {
                                m1 = d[j, i];
                            }
                        }
                        for (j = 1; j <= n; j++)
                        {
                            if (d[j, i] == -1)
                            {
                                c++;
                            }
                        }
                        if (c == n)
                        {
                            max[i] = -1;
                        }
                        else
                            max[i] = m1;
                    }
                    for (i = 1; i <= n; i++)
                    {
                        if (max[i] < obj_len && max[i] >= 0)
                        {
                            Q[l++] = i;
                            count++;
                        }
                    }
                    for (i = 1; i <= l; i++)
                    {
                        for (j = 1; j <= n; j++)
                        {
                            d[Q[i], j] = -1;
                            d[j, Q[i]] = -1;
                        }
                    }
                    k1[k] = l;
                    for (i = 1; i < l; i++)
                    {
                        front[k, i] = Q[i];
                    }
                    k++;
                    if (count >= n)
                    {
                        break;
                    }
                }

                string kkk = ",";
                kkk = "," + k1[k - 1] + "," + (k - 1);
                //tb_droplocation.Text = kkk.ToString();
                result = new int[k1[k - 1]];
                for (i = 1; i < k1[k - 1]; i++)
                {
                    result[i] = front[k - 1, i];

                }
                string ass = " ";
                for (i = 1; i < result.Length; i++)
                {
                    ass = ass + "," + result[i];
                }
                //tb_pickuplocation.Text = ass.ToString();
                return (result);

            }
            //Algorithm 2
            public int[] cnstDomMat(double[,] s, int m, int n, int[] obj)
            {
                double[] a;
                int flag = 0;
                int[] result;
                int[,] summation_cmp;
                int[,] cmp;
                int i, j, k, l;
                a = new double[n + 1];
                cmp = new int[n + 1, n + 1];
                summation_cmp = new int[n + 1, n + 1];
                string pss = ",";
                for (i = 1; i <= m; i++)
                {
                    flag = 0;
                    for (j = 1; j <= n; j++)
                    {
                        a[j] = s[i, j];
                    }
                    for (int o = 1; o < obj.Length; o++)
                    {
                        if (i == obj[o])
                        {
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 1)
                    {
                        pss = pss + " ," + i;
                        cmp = cnstCmpMat(a, n); //algorithm 1 fun call
                        for (k = 1; k <= n; k++)
                        {
                            for (l = 1; l <= n; l++)
                            {
                                summation_cmp[k, l] += cmp[k, l];
                            }
                        }
                    }

                }
                // tb_droplocation.Text = pss.ToString();
                string pps = " ";
                for (i = 1; i < obj.Length; i++)
                {
                    pps = pps + ", " + obj[i];
                }
                pps = pps + " ," + obj.Length;
                // tb_pickuplocation.Text = pps.ToString();

                result = dda_ns(summation_cmp, m, n, obj.Length - 1); //algorithm 3 fun call
                return (result);
            }
            public int[,] cnstCmpMat(double[] a, int n)
            {
                int i, j;
                int[,] c;
                int[] b;
                int t1;
                double t;
                c = new int[n + 1, n + 1];
                b = new int[n + 1];
                for (i = 1; i <= n; i++)
                {
                    b[i] = i;
                }
                for (i = 1; i <= n; i++)
                {
                    for (j = 1; j <= n; j++)
                    {
                        if (a[i] < a[j])
                        {
                            t = a[i];
                            a[i] = a[j];
                            a[j] = t;
                            t1 = b[i];
                            b[i] = b[j];
                            b[j] = t1;
                        }
                    }
                }
                for (i = 1; i <= n; i++)
                {
                    c[b[1], i] = 1;
                }
                for (i = 2; i <= n; i++)
                {
                    if (a[i] == a[i - 1])
                    {
                        for (j = 1; j <= n; j++)
                        {
                            c[b[i], j] = c[b[i - 1], j];
                        }
                    }
                    else
                    {
                        for (j = i; j <= n; j++)
                        {
                            c[b[i], b[j]] = 1;
                        }
                    }
                }
                return c;
            }

        }
    }