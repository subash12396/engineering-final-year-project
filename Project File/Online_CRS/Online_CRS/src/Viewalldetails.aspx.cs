﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace Online_CRS
{
    public partial class Viewalldetails : System.Web.UI.Page
    {
        public UpdateDB UpdateDB
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection())
            {

                con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                con.Open();
                SqlCommand com = new SqlCommand("select * from car_details", con);
                //tb_view.Text=com.ExecuteScalar().ToString();
                SqlDataReader nwReader = com.ExecuteReader();
                String data ="";
                while (nwReader.Read())
                {
                    data = data +"\t" + nwReader["car_name"].ToString();
                    data = data +"\t" + nwReader["comfort"].ToString();
                    data = data + "\t" + nwReader["safeties"].ToString();
                    data = data + "\t" + nwReader["quality"].ToString();
                    data = data + "\t" + nwReader["media_feature"].ToString();
                    data = data + "\t" + nwReader["city_mileage"].ToString();
                    data = data + "\t" + nwReader["high_mileage"].ToString();
                    data = data + "\t" + nwReader["Cargo_volume"].ToString();
                    data = data + "\t" + nwReader["Seating_capacity"].ToString();
                    data = data + "\t" + nwReader["Speed"].ToString();
                    data = data + "\t" + nwReader["Price"].ToString();
                    data = data + "\n";

                }
                tb_viewall.Text = data.ToString();
                nwReader.Close();
                com.ExecuteNonQuery();

            }
        }

        protected void b_done_Click(object sender, EventArgs e)
        {
            Server.Transfer("UpdateDB.aspx");
        }
    }
}