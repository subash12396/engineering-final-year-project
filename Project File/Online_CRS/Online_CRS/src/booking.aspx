﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site(booking).Master" AutoEventWireup="true" CodeBehind="booking.aspx.cs" Inherits="Online_CRS.booking" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 883px;
        }
        .auto-style2 {
            width: 297px;
        }
        .auto-style18 {
            width: 521px;
        }
        .auto-style19 {
            width: 757px;
        }
        .auto-style20 {
            width: 409px;
        }
        .auto-style21 {
            width: 306px;
        }
        .auto-style22 {
            width: 344px;
        }
        .auto-style11 {
            height: 66px;
            width: 755px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
</div>
<div>

        <table class="auto-style1">
            <tr>
                <td class="auto-style13">
                </td>
                <td class="auto-style22">
                </td>
            </tr>
            <tr>
                <td class="auto-style14">
                    <asp:Label ID="Label2" style="margin-left:200px" runat="server" Text="Pickup Location"></asp:Label>
                </td>
                <td class="auto-style22">
                    <asp:TextBox ID="tb_pickuplocation" runat="server" Height="42px" TextMode="MultiLine" Width="300px" ></asp:TextBox>
                </td>
                <td>
                     <asp:DropDownList ID="ddl_pickuplocation" runat="server" AutoPostBack="True" Height="35px" Width="165px">
                        <asp:ListItem>Ariyalur</asp:ListItem>
                        <asp:ListItem>Chennai</asp:ListItem>
                        <asp:ListItem>Coimbatore</asp:ListItem>
                        <asp:ListItem>Cuddalore</asp:ListItem>
                        <asp:ListItem>Dharmapuri</asp:ListItem>
                        <asp:ListItem>Dindigul</asp:ListItem>
                        <asp:ListItem>Erode</asp:ListItem>
                        <asp:ListItem>Kanchipuram</asp:ListItem>
                         <asp:ListItem>Kanniyakumari</asp:ListItem>
                         <asp:ListItem>Karur</asp:ListItem>
                         <asp:ListItem>Krishnagiri</asp:ListItem>
                         <asp:ListItem>Madurai</asp:ListItem>
                         <asp:ListItem>Nagapattinam</asp:ListItem>
                         <asp:ListItem>Namakkal</asp:ListItem>
                         <asp:ListItem>Perambalur</asp:ListItem>
                         <asp:ListItem>Pudukkottai</asp:ListItem>
                         <asp:ListItem>Ramanathapuram</asp:ListItem>
                         <asp:ListItem>Salem</asp:ListItem>
                         <asp:ListItem>Sivaganga</asp:ListItem>
                         <asp:ListItem>Thanjavur</asp:ListItem>
                         <asp:ListItem>Theni</asp:ListItem>
                         <asp:ListItem>Thoothukudi</asp:ListItem>
                         <asp:ListItem>Tiruchirappalli</asp:ListItem>
                         <asp:ListItem>Tirunelveli</asp:ListItem>
                         <asp:ListItem>Tiruppur</asp:ListItem>
                         <asp:ListItem>Tiruvallur</asp:ListItem>
                         <asp:ListItem>Tiruvannamalai</asp:ListItem>
                         <asp:ListItem>Tiruvarur</asp:ListItem>
                         <asp:ListItem>Vellore</asp:ListItem>
                         <asp:ListItem>Viluppuram</asp:ListItem>
                         <asp:ListItem>Virudhunagar</asp:ListItem>
                    </asp:DropDownList>

                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tb_pickuplocation" ErrorMessage="*" ForeColor="#000066"></asp:RequiredFieldValidator>

                </td>
            </tr>
            <tr>
                <td class="auto-style14">
                    <asp:Label ID="Label3" style="margin-left:200px" runat="server" Text="Drop Location"></asp:Label>
                    </td>
                <td class="auto-style22">
                   
                    <asp:TextBox ID="tb_droplocation" runat="server" Height="42px" TextMode="MultiLine" Width="300px"></asp:TextBox>
                   
                </td>
                <td>
                     <asp:DropDownList ID="ddl_droplocation" runat="server" AutoPostBack="True" Height="35px" Width="165px">
                        <asp:ListItem>Ariyalur</asp:ListItem>
                        <asp:ListItem>Chennai</asp:ListItem>
                        <asp:ListItem>Coimbatore</asp:ListItem>
                        <asp:ListItem>Cuddalore</asp:ListItem>
                        <asp:ListItem>Dharmapuri</asp:ListItem>
                        <asp:ListItem>Dindigul</asp:ListItem>
                        <asp:ListItem>Erode</asp:ListItem>
                        <asp:ListItem>Kanchipuram</asp:ListItem>
                         <asp:ListItem>Kanniyakumari</asp:ListItem>
                         <asp:ListItem>Karur</asp:ListItem>
                         <asp:ListItem>Krishnagiri</asp:ListItem>
                         <asp:ListItem>Madurai</asp:ListItem>
                         <asp:ListItem>Nagapattinam</asp:ListItem>
                         <asp:ListItem>Namakkal</asp:ListItem>
                         <asp:ListItem>Perambalur</asp:ListItem>
                         <asp:ListItem>Pudukkottai</asp:ListItem>
                         <asp:ListItem>Ramanathapuram</asp:ListItem>
                         <asp:ListItem>Salem</asp:ListItem>
                         <asp:ListItem>Sivaganga</asp:ListItem>
                         <asp:ListItem>Thanjavur</asp:ListItem>
                         <asp:ListItem>Theni</asp:ListItem>
                         <asp:ListItem>Thoothukudi</asp:ListItem>
                         <asp:ListItem>Tiruchirappalli</asp:ListItem>
                         <asp:ListItem>Tirunelveli</asp:ListItem>
                         <asp:ListItem>Tiruppur</asp:ListItem>
                         <asp:ListItem>Tiruvallur</asp:ListItem>
                         <asp:ListItem>Tiruvannamalai</asp:ListItem>
                         <asp:ListItem>Tiruvarur</asp:ListItem>
                         <asp:ListItem>Vellore</asp:ListItem>
                         <asp:ListItem>Viluppuram</asp:ListItem>
                         <asp:ListItem>Virudhunagar</asp:ListItem>
                    </asp:DropDownList>

                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tb_droplocation" ErrorMessage="*" ForeColor="#000066"></asp:RequiredFieldValidator>
                   
                </td>
            </tr>
            <tr>
                <td class="auto-style14">
                    <asp:Label ID="Label4" runat="server" style="margin-left:150px" Text="Pick more places to visit "></asp:Label>
                </td>
                <td class="auto-style22">
                    <asp:Button ID="b_addplaces" runat="server" Text="Add Places"  CausesValidation="False" />
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                   
                    <cc:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panl1" TargetControlID="b_addplaces" CancelControlID="b_back"></cc:ModalPopupExtender>
                         <asp:Panel Id="Panl1" runat="server"  align="center" style = "display:none;background-color:lightgray">
                            <iframe id="frm1" style=" width: 550px; height: 400px;border:groove" src="addplaces.aspx" runat="server"></iframe>
                            <br/>
                            <asp:Button ID="b_back" Style="margin-left:100px" runat="server" Text="Back"  CausesValidation="False"  />
                          </asp:Panel>
                    
                </td>
            </tr>
            <tr>
                <td class="auto-style14">
                    <asp:Label ID="Label5" style="margin-left:200px" runat="server" Text="Pickup Time"></asp:Label>
                </td>
                <td class="auto-style22">
                   
                    <asp:TextBox ID="tb_pickuptime" runat="server" TextMode="Time"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="tb_pickuptime" ErrorMessage="*" ForeColor="#000066"></asp:RequiredFieldValidator>
                   
                </td>
            </tr>
            <tr>
                <td class="auto-style14">
                    <asp:Label ID="Label7" style="margin-left:200px" runat="server" Text="Pickup Date"></asp:Label>
                </td>
                <td class="auto-style22">
                   
                    <asp:TextBox ID="tb_pickupdate" runat="server" TextMode="Date"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="tb_pickupdate" ErrorMessage="*" ForeColor="#000066"></asp:RequiredFieldValidator>
                   
                </td>
            </tr>
            <tr>
                <td class="auto-style14">
                    <asp:Label ID="Label6" style="margin-left:200px" runat="server" Text="Dropup Time"></asp:Label>
                </td>
                <td class="auto-style22">
                   <asp:TextBox ID="tb_droptime"  runat="server" TextMode="Time"></asp:TextBox>
                    
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="tb_droptime" ErrorMessage="*" ForeColor="#000066"></asp:RequiredFieldValidator>
                   
                </td>
            </tr>
            <tr>
                <td class="auto-style14">
                    <asp:Label ID="Label8" style="margin-left:200px" runat="server" Text="Drop Date"></asp:Label>
                </td>
                <td class="auto-style22">
                   
                    <asp:TextBox ID="tb_dropdate" runat="server" TextMode="Date"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="tb_dropdate" ErrorMessage="*" ForeColor="#000066"></asp:RequiredFieldValidator>
                   
                </td>
            </tr>
            <tr>
                <td class="auto-style14">
                    <asp:Label ID="Label9" style="margin-left:200px" runat="server" Text="Driver Required"></asp:Label>
                </td>
                <td class="auto-style22">
                   
                    <asp:RadioButton ID="rb_driverreqyes" runat="server" GroupName="driver_requried" Text="Yes"  />
                    <asp:RadioButton ID="rb_driverreqno" style="margin-left:50px" runat="server" GroupName="driver_requried" Text="No" />
                </td>
            </tr>
            <tr>
                <td class="auto-style15">
                </td>
                <td class="auto-style22">
                   
                </td>
            </tr>
        </table>

   </div>
    <div>

        <table class="auto-style11">
            <tr>
                <td class="auto-style16">
                    <asp:Label ID="Label10" style="margin-left:200px" runat="server" Text="Driver Gender"></asp:Label>
                </td>
                <td class="auto-style20">
                    <asp:RadioButton ID="rb_lady" runat="server" GroupName="gender" Text="Lady" />
                    <asp:RadioButton ID="rb_gents" style="margin-left:40px" runat="server" GroupName="gender" Text="Gents" />
                </td>
            </tr>
            <tr>
                <td class="auto-style12">
                    <asp:Label ID="Label11" style="margin-left:200px" runat="server" Text="Driver Experience"></asp:Label>
                </td>
                <td class="auto-style20">
                    <asp:RadioButton ID="rb_5years"  runat="server" GroupName="year" Text="5 years +" />
                    <asp:RadioButton ID="rb_8years" style="margin-left:20px" runat="server" GroupName="year" Text="8 years +" />
                    <asp:RadioButton ID="rb_14years" style="margin-left:20px" runat="server" GroupName="year" Text="14 years +" />
                </td>
            </tr>
            <tr>
                <td class="auto-style12">
                    &nbsp;</td>
                <td class="auto-style20">
                    &nbsp;</td>
            </tr>
        </table>

    </div>
    <div>

        <asp:Label ID="Label12"  runat="server" Text="Select Car's Objectives Based On Your Interest" Font-Size="18pt"></asp:Label>

    </div>
     
 <asp:updatepanel runat="server">
 <ContentTemplate>
    <div>


        <table class="auto-style18" style="position: relative; top: 0px; left: 0px">
            <tr>
                <td class="auto-style2">
                    &nbsp;</td>
                <td class="auto-style21">
                    <asp:CheckBoxList ID="cbl_parameters" runat="server" AutoPostBack="True" Width="182px">
                        <asp:ListItem>Comfort</asp:ListItem>
                        <asp:ListItem>Safety</asp:ListItem>
                        <asp:ListItem>Quality</asp:ListItem>
                        <asp:ListItem>Media Features</asp:ListItem>
                        <asp:ListItem>City Mileage</asp:ListItem>
                        <asp:ListItem>Highway Mileage</asp:ListItem>
                        <asp:ListItem>Cargo Volume</asp:ListItem>
                        <asp:ListItem>Seating Capacity</asp:ListItem>
                        <asp:ListItem>Speed</asp:ListItem>
                        <asp:ListItem>Cost(Budget)</asp:ListItem>
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>
               </td>
            </tr>
        </table>

    </div>
     </ContentTemplate>
 </asp:updatepanel>
 <div>
      <asp:Button ID="b_done1" Style="margin-left:600px" runat="server" Text="Done" OnClick="b_done1_Click1" />
 </div>
</asp:Content>
