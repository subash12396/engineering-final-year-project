﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace Online_CRS
{
    public partial class adminsignup : System.Web.UI.Page
    {
        static string com_name, admin_name, admin_email, admin_pass, admin_conpass, com_add, com_phone;

        public adminlogin adminlogin
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public adminhome adminhome
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        protected void b_adminlogin_Click(object sender, EventArgs e)
        {
            Server.Transfer("adminlogin.aspx");
        }

        protected void b_adminsignup_Click1(object sender, EventArgs e)
        {
            com_name = tb_companyname.Text.ToString();
            admin_name = tb_adminname.Text.ToString();
            admin_email = tb_adminemailid.Text.ToString();
            admin_pass = tb_adminpassword.Text.ToString();
            admin_conpass = tb_adminconpassword.Text.ToString();
            com_add = tb_comaddress.Text.ToString();
            com_phone = tb_adminphoneno.Text.ToString();
            using (SqlConnection con = new SqlConnection())
            {

                con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                con.Open();
                SqlCommand com = new SqlCommand("insert admindetails values(@com_name,@admin_name,@admin_pass,@admin_conpass,@admin_email,@com_add,@com_phone)", con);
                com.Parameters.AddWithValue("@com_name", com_name);
                com.Parameters.AddWithValue("@admin_name", admin_name);
                com.Parameters.AddWithValue("@admin_pass", admin_pass);
                com.Parameters.AddWithValue("@admin_conpass", admin_conpass);
                com.Parameters.AddWithValue("@admin_email", admin_email);
                com.Parameters.AddWithValue("@com_add", com_add);
                com.Parameters.AddWithValue("@com_phone", com_phone);
                com.ExecuteNonQuery();
            }
            Server.Transfer("adminhome.aspx");
        }
    }
}