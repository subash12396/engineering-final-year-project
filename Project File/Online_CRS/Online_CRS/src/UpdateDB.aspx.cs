﻿using System;
using System.Data.SqlClient;

namespace Online_CRS
{
    public partial class UpdateDB : System.Web.UI.Page
    {
        static string car_name,fueltype;
        static int car_id;
        static double comfort, safety, quality, media_feature, city_mileage, high_mileage, cargo_volume, seating_cap, speed, price;
        static string carname;

        public Viewdetails Viewdetails
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public Viewalldetails Viewalldetails
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        protected void b_view_Click(object sender, EventArgs e)
        {

            car_id = Convert.ToInt32(tb_view_carid.Text.ToString());
            Session["car_id_view"] = car_id;
             Server.Transfer("Viewdetails.aspx");
           
        }

        protected void b_delete_Click(object sender, EventArgs e)
        {
            car_id = Convert.ToInt32(tb_del_carid.Text);
            using (SqlConnection con = new SqlConnection())
            {

                con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                con.Open();
                SqlCommand com = new SqlCommand("delete from car_details where car_id=@car_id",con);
                com.Parameters.AddWithValue("@car_id", car_id);
                try
                {
                    com.ExecuteNonQuery();
                    string script = "alert('Car is Deleted.');";
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
                }
                    catch (Exception ex) {
                    
                }

            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void b_viewall_Click(object sender, EventArgs e)
        {
            Server.Transfer("Viewalldetails.aspx");
        }

        protected void b_update_Click(object sender, EventArgs e)
        {
            car_id = Convert.ToInt32(tb_carid.Text);
            car_name = tb_carname.Text.ToString();
            fueltype = tb_fueltype.Text.ToString();
            carname = car_name + "  " + fueltype;
            comfort =Convert.ToDouble(tb_comfort.Text);
            safety = Convert.ToDouble(tb_safety.Text);
            quality = Convert.ToDouble(tb_quality.Text);
           media_feature = Convert.ToDouble(tb_mediafeatures.Text);
            city_mileage = Convert.ToDouble(tb_citymileage.Text);
            high_mileage = Convert.ToDouble(tb_highwaymileage.Text);
            cargo_volume = Convert.ToDouble(tb_cargovolume.Text);
            seating_cap = Convert.ToDouble(tb_seatingcap.Text);
            speed = Convert.ToDouble(tb_speed.Text);
            price = Convert.ToDouble(tb_price.Text);
            using (SqlConnection con = new SqlConnection())
            {

                con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                con.Open();
                SqlCommand com = new SqlCommand("insert car_details values(@car_id,@car_name,@comfort,@safties,@quality,@media_feature,@city_mileage,@high_mileage,@cargo_volume,@seating_capacity,@speed,@price)", con);
                com.Parameters.AddWithValue("@car_id", car_id);
                com.Parameters.AddWithValue("@car_name", carname);
                
                com.Parameters.AddWithValue("@comfort", comfort);
                com.Parameters.AddWithValue("@safties", safety);
                com.Parameters.AddWithValue("@quality", quality);
                com.Parameters.AddWithValue("@media_feature",media_feature);
                com.Parameters.AddWithValue("@city_mileage", city_mileage);
                com.Parameters.AddWithValue("@high_mileage", high_mileage);
                com.Parameters.AddWithValue("@cargo_volume", cargo_volume);
                com.Parameters.AddWithValue("@seating_capacity", seating_cap);
                com.Parameters.AddWithValue("@speed", speed);
                com.Parameters.AddWithValue("@price", price);
                try
                {
                    com.ExecuteNonQuery();
                    string script = "alert('A New Car is Added.');";
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
                }
                catch(Exception ec) {
                    string script = "alert('Car_id Already Present');";
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
                }

            }

        }
    }
}