﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site(user login signup).Master" AutoEventWireup="true" CodeBehind="Confirmation.aspx.cs" Inherits="Online_CRS.Confirmation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 853px;
            height: 455px;
        }
        .auto-style2 {
            height: 26px;
        }
        .auto-style3 {
            height: 25px;
        }
        .auto-style4 {
            height: 22px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
    
        <asp:Label ID="Label1" runat="server" style="margin-left:200px" Text="Confirmation"></asp:Label>
    
    </div>
        <div>

              <table class="auto-style1">
                  <tr>
                      <td class="auto-style5">&nbsp;</td>
                      <td class="auto-style2">

            <asp:Button ID="bt_logout" runat="server" style="margin-left:600px" Text="Logout" OnClick="bt_logout_Click"  />
                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style3">

            <asp:Label ID="lbl_source" runat="server" Text="Source:"></asp:Label>

                      </td>
                      <td class="auto-style4">
            <asp:TextBox ID="tb_source" runat="server" Width="170px"></asp:TextBox>

                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style5">

            <asp:Label ID="lbl_destination" runat="server" Text="Destination:"></asp:Label>
                      </td>
                      <td class="auto-style2">
            <asp:TextBox ID="tb_destination" runat="server" Width="170px"></asp:TextBox>

                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style5">

            <asp:Label ID="lbl_selectedcity" runat="server" Text="Selected city to visit:"></asp:Label>
                          <br />
                      </td>
                      <td class="auto-style2">
            <asp:TextBox ID="tb_selectedcity" runat="server" TextMode="MultiLine" Width="170px"></asp:TextBox>

                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style4">

            <asp:Label ID="lbl_timedate" runat="server" Text="Time :"></asp:Label>
                      </td>
                      <td class="auto-style4">
            <asp:TextBox ID="tb_fromtime" runat="server" Width="170px"></asp:TextBox>
                          &nbsp;<asp:Label ID="Label3" runat="server" Text="TO"></asp:Label>
                          <asp:TextBox ID="tb_totime" runat="server"  Width="168px"></asp:TextBox>

                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style5">

            <asp:Label ID="lbl_timedate0" runat="server" Text="Date:"></asp:Label>
                      </td>
                      <td class="auto-style2">
            <asp:TextBox ID="tb_fromdate" runat="server" Width="170px"></asp:TextBox>

                          <asp:Label ID="Label4" runat="server" Text="TO"></asp:Label>
                          <asp:TextBox ID="tb_todate" runat="server"  Width="168px"></asp:TextBox>

                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style5">

            <asp:Label ID="lbl_driverdetails" runat="server" Text="Driver details:"></asp:Label>
                      </td>
                      <td class="auto-style2">
            <asp:TextBox ID="tb_driverdetails" runat="server" Width="361px"></asp:TextBox>

                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style5">

            <asp:Label ID="lbl_selectedcar" runat="server" Text="Selected car:"></asp:Label>
                          <br />
                      </td>
                      <td class="auto-style2">
            <asp:TextBox ID="tb_selectedcar" runat="server" Width="358px"></asp:TextBox>

                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style5">

            <asp:Label ID="lbl_amount" runat="server" Text="Amount:"></asp:Label>

                      </td>
                      <td class="auto-style2">

            <asp:TextBox ID="tb_amount" runat="server" Width="166px"></asp:TextBox>

                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style5">

                          &nbsp;</td>
                      <td class="auto-style2">

                          &nbsp;</td>
                  </tr>
                  <tr>
                      <td class="auto-style5">&nbsp;</td>
                      <td class="auto-style2">
                          <asp:Button ID="bt_modify" runat="server" Text="Modify" style="margin-left:300px" OnClick="bt_modify_Click"/>
&nbsp;<asp:Button ID="bt_booknow" runat="server" Text="Book  Now" OnClick="bt_booknow_Click" />
                      </td>
                  </tr>
              </table>

            </div>
</asp:Content>
