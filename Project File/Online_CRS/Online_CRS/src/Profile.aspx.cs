﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
namespace Online_CRS
{
    public partial class Profile : System.Web.UI.Page
    {
        static string emailid, name, add, phone;

        public Settings Settings
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public MyReservation MyReservation
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["emailid"] != null)
                emailid = Session["emailid"].ToString();
            using (SqlConnection con = new SqlConnection())
            {

                con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                con.Open();
                SqlCommand com = new SqlCommand("select * from userdetails where user_emailid=@emailid", con);
                com.Parameters.AddWithValue("@emailid", emailid);
                SqlDataReader nwReader = com.ExecuteReader();
                while (nwReader.Read())
                {
                    name = nwReader["username"].ToString();
                    add = nwReader["user_add"].ToString();
                    phone = nwReader["user_phone_no"].ToString();
                }
                nwReader.Close();
                com.ExecuteNonQuery();
            }
            tb_name.Text = name;
            tb_emailid.Text = emailid;
            tb_address.Text = add;
            tb_phone.Text = phone;

        }
        protected void bt_myreservation_Click(object sender, EventArgs e)
        {
            Server.Transfer("MyReservation.aspx");
        }

        protected void bt_settings_Click(object sender, EventArgs e)
        {
            Server.Transfer("Settings.aspx");
        }

      


        protected void bt_okay_Click(object sender, EventArgs e)
        {
            Server.Transfer("booking.aspx");
        }

        
    }
}