﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Online_CRS
{
    public partial class Site_booking_ : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void b_logout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Server.Transfer("userhome.aspx");
        }

        protected void b_profile_Click(object sender, EventArgs e)
        {
            Server.Transfer("Profile.aspx");
        }
    }
}