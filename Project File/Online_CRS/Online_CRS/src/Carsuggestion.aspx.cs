﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace Online_CRS
{
    public partial class Carsuggestion : System.Web.UI.Page
    {
        string[] car_name;
        int i;
        int[] car_id;
        double[] price;
        string selected_car;
        string selected_price;
        int selected_car_id,order_id;
        string route;

        public Confirmation Confirmation
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            if (Session["car_name"] != null)
                car_name = Session["car_name"] as string[];
            if (Session["car_id"] != null)
                car_id = Session["car_id"] as int[];
            if (Session["Price"] != null)
                price = Session["Price"] as double[];
            if (Session["order_id"] != null)
                order_id =Convert.ToInt32( Session["order_id"]);
            if (Session["route"] != null)
                route = Session["route"].ToString();
            tb_route.Text = route.ToString();

        }
        protected void bt_done_Click(object sender, EventArgs e)
        {
            selected_car = rbl_need.SelectedItem.ToString();
            selected_price = tb_amount.Text.ToString();
            selected_car_id = car_id[rbl_need.SelectedIndex +1];
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = @"Data Source=SUBASH-PC\MSSQLEXPRESS; Initial Catalog=crp;Integrated Security=True";
                con.Open();
                SqlCommand com = new SqlCommand("update orderdetails set selected_car=@selected_car,car_id=@selected_car_id ,amount=@selected_price where order_id=@order_id", con);
                com.Parameters.AddWithValue("@selected_car", selected_car);
                com.Parameters.AddWithValue("@selected_car_id", selected_car_id);
                com.Parameters.AddWithValue("@selected_price", selected_price);
                com.Parameters.AddWithValue("@order_id", order_id);
                com.ExecuteNonQuery();
            }
            
            Server.Transfer("Confirmation.aspx");
         
           
        }

        protected void b_getcar_Click(object sender, EventArgs e)
        {
            rbl_need.Visible = false;

            for (i = 1; i < car_name.Length-1; i++)
            {
                rbl_need.Items.Add(new ListItem(car_name[i], car_name[i]));
            }
            rbl_need.Visible = true;
         }

        protected void rbl_need_SelectedIndexChanged(object sender, EventArgs e)
        {
            int k = rbl_need.SelectedIndex;
            tb_amount.Text = Convert.ToDouble(price[k + 1]).ToString();
           
        }
    }

       
}