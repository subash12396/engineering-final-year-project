﻿use crp
alter table car_details add car_name varchar(30)
alter table car_details add fueltype varchar(10)
alter table car_details add comfort  float
alter table car_details add safeties float
alter table car_details add quality float
alter table car_details add media_feature  float
alter table car_details add city_mileage  float
alter table car_details add high_mileage float
alter table car_details add cargo_volume  float
alter table car_details add seating_capacity  float
alter table car_details add speed  float
alter table car_details add price  float